const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'dailyjob',
  {
    idCliente: {
        type: Sequelize.INTEGER,
    },
    venta: {
        type: Sequelize.STRING,
        length: 2
    },
    noVenta: {
        type: Sequelize.STRING,
        length: 50
    },
    valorPedido: {
        type: Sequelize.STRING,
        length: 11
    },
    obsVenta: {
        type: Sequelize.STRING,
        length: 40
    },
    nit: {
        type: Sequelize.STRING,
        length: 100
    },
    Latitude: {
        type: Sequelize.STRING,
        length: 40
    },
    Longitude: {
        type: Sequelize.STRING,
        length: 40
    },
    savedBy: {
        type: Sequelize.STRING,
        length: 20
    },
    ingresoFH: {
        type: Sequelize.STRING,
    },
    imgRuta: {
        type: Sequelize.STRING,
        length: 900
    },
    taskID: {
        type: Sequelize.INTEGER
    },
    form: {
        type: Sequelize.STRING,
        length: 20
    },
    vendedor: {
        type: Sequelize.STRING,
        length: 50
    },
    distribuidor: {
        type: Sequelize.STRING,
        length: 50
    },
    prodAbrVen: {
        type: Sequelize.STRING,
        length: 500
    },

  },
  {
    timestamps: false,
    freezeTableName: true,
  }
)
