var sqlMap = {

  SaleFormQ : {

    GetCustNew : 'Call GetCustNew(:nombreNegocio,:estrato,:telefono,:contacto,:cargo,:correo,:pais,:departamento,:ciudadPoblacion,:territorio,:nroComunaZona,:nombreComZon,:barrio,:clasificacion,:tipologia,:ordenRuta,:comentarios,:direccion,:creadoPor,:pubExte,:pubInte,:exhibi,:nit,:taskID,:form,:lat,:lng,:origenDatos,:prodAbrasivos,:lineasS,:promCompr,:UnCorteF,:marcas,:unFlapD,:marcasFD,:promLS,:maLijS,:numVerParPer);',

    GetCustNewNoPart : 'Call GetCustNewNoPart(:nombreNegocio,:estrato,:telefono,:contacto,:cargo,:correo,:pais,:departamento,:ciudadPoblacion,:territorio,:nroComunaZona,:nombreComZon,:barrio,:clasificacion,:tipologia,:ordenRuta,:comentarios,:direccion,:creadoPor,:pubExte,:pubInte,:exhibi,:nit,:taskID,:form,:lat,:lng,:origenDatos,:prodAbrasivos,:lineasS,:promCompr,:UnCorteF,:marcas,:unFlapD,:marcasFD,:promLS,:maLijS);',

    UpdateByID             : "Call UpdateByID(:id,:nombreNegocio, :estrato, :telefono, :contacto, :cargo, :correo, :pais, :departamento, :ciudadPoblacion, :territorio, :nroComunaZona, :nombreComZon, :barrio, :clasificacion, :tipologia, :ordenRuta, :comentarios, :direccion, :modificadoPor, :pubExte, :pubInte, :exhibi, :nit, :taskID, :form,:lat,:lng, :origenDatos, :foto,:prodAbrasivos,:lineasS,:promCompr,:UnCorteF,:marcas,:unFlapD,:marcasFD,:promLS,:maLijS,:numVerParPer,:pIdRoute)",

    UpdateByID_Route : "Call UpdateByID_Route(:id,:nombreNegocio, :estrato, :telefono, :contacto, :cargo, :correo, :pais, :departamento, :ciudadPoblacion, :territorio, :nroComunaZona, :nombreComZon, :barrio, :clasificacion, :tipologia, :ordenRuta, :comentarios, :direccion, :modificadoPor, :pubExte, :pubInte, :exhibi, :nit, :taskID, :form,:lat,:lng, :origenDatos, :foto,:prodAbrasivos,:lineasS,:promCompr,:UnCorteF,:marcas,:unFlapD,:marcasFD,:promLS,:maLijS,:numVerParPer,:pIdRoute)",

    UpdateByID_R : "Call UpdateByID(:nombreNegocio, :estrato, :telefono, :contacto, :cargo, :correo, :pais, :departamento, :ciudadPoblacion, :territorio, :nroComunaZona, :nombreComZon, :barrio, :clasificacion, :tipologia, :ordenRuta, :comentarios, :direccion, :modificadoPor, :pubExte, :pubInte, :exhibi, :nit, :taskID, :form,:lat,:lng, :origenDatos, :foto,:prodAbrasivos,:lineasS,:promCompr,:UnCorteF,:marcas,:unFlapD,:marcasFD,:promLS,:maLijS)",

    delete_by_NIT : 'delete from fichacliente where nit = ? ',

  },

}

module.exports = sqlMap;
