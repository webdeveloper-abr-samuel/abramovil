var express = require('express');
const sequelize = require('sequelize');
const fs = require('fs');
const Op = sequelize.Op;
const Cust = require('../models/Cust');
const jwt = require('jsonwebtoken');
const config = require('./config/config');

var router = express.Router();

router.post('/data', (req, res) => {
    var params = req.body;

    Cust.sequelize.query(
            "select count(*) as afectados, distribuidor from fichacliente inner join gestiondiaria on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%bogota%' and (barrio like '%" + params.zona + "%' or nombreComZon like '%" + params.zona + "%') and distribuidor not like ''  group by distribuidor order by afectados desc;", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/sumZona', (req, res) => {
    var params = req.body;
    query = "select sum(valorPedido) as perdidas, distribuidor from fichacliente inner join gestiondiaria on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%bogota%' and (barrio like '%" + params.zona + "%' or nombreComZon like '%" + params.zona + "%') and distribuidor not like '' group by distribuidor order by perdidas desc;"

    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/clienteNoGestion', (req, res) => {
    var params = req.body;
    var query = '';
    if (params.zona) {
        query = "select count(distinct nombreNegocio) as negocios from fichacliente left join gestiondiaria on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%" + params.ciudad + "%' and (barrio like '%" + params.zona + "%' or nombreComZon like '%" + params.zona + "%') and (telefono not like '' or correo not like '') and distribuidor = '';"
    } else {
        query = "select count(distinct nombreNegocio) as negocios from fichacliente left join gestiondiaria on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%" + params.ciudad + "%' and (telefono not like '' or correo not like '') and distribuidor = '';"
    }

    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/clienteClasificacion', (req, res) => {
    var params = req.body;
    var query = '';
    query = "select count(*) as cantidad, clasificacion from fichacliente inner join gestiondiaria on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%" + params.ciudad + "%' and clasificacion not like ''  group by clasificacion order by clasificacion asc;"

    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/GrafFlap', (req, res) => {
    var params = req.body;
    var query = '';
    query = "select count(unFlapD) as cantidad_clientes, unFlapD as Flap from fichacliente where ciudadPoblacion like '%" + params.ciudad + "%' and unFlapD not like '' and unFlapD not like 'no consume Flap Disc' group by unFlapD order by Flap desc;"


    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/GrafCorte', (req, res) => {
    var params = req.body;
    var query = '';
    query = "select count(UnCorteF) as cantidad_clientes, UnCorteF as Corte from fichacliente where ciudadPoblacion like '%" + params.ciudad + "%' and UnCorteF not like '' and UnCorteF not like 'no consume corte fino.' group by UnCorteF order by UnCorteF desc;"


    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/GrafLija', (req, res) => {
    var params = req.body;
    var query = '';
    query = "select count(promLS) as cantidad_clientes, promLS as Lija from fichacliente where ciudadPoblacion like '%" + params.ciudad + "%' and promLS not like '' and promLS not like 'no consume lija en seco' group by promLS order by promLS desc;"


    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/GrafAAD', (req, res) => {
    var params = req.body;
    var date = new Date();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var query = '';
    if (month < 10) {
        month = '0' + month
    }

    query = `select distinct sum(valorPedido) as total, sum(case when venta = 'no' then 1 else 0 end) as no_venta, 
    sum(case when venta = 'si' then 1 else 0 end) as si_venta, savedBy as usuario from fichacliente inner join gestiondiaria 
    on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%` + params.ciudad + `%' and ingresoFH like '%` + year + `-` + month + `%' group by savedBy order by total desc;`

    // query = `select distinct sum(valorPedido) as total, sum(case when venta = 'no' then 1 else 0 end) as no_venta, 
    // sum(case when venta = 'si' then 1 else 0 end) as si_venta, savedBy as usuario from fichacliente inner join gestiondiaria 
    // on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%` + params.ciudad + `%' and ingresoFH like '%` + year + `-02%' group by savedBy order by total desc;`

    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/GrafAAD2', (req, res) => {
    var params = req.body;
    var date = new Date();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var query = '';
    if (month < 10) {
        month = '0' + month
    }

    query = `select savedBy as usuario, sum(valorPedido) as total, sum(case when venta = 'no' then 1 else 0 end) as no_venta, 
    sum(case when venta = 'si' then 1 else 0 end) as si_venta,
    count(distinct(case when venta = 'si' then nombreNegocio end)) as venta_unica from fichacliente inner join gestiondiaria 
    on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%` + params.ciudad + `%' and ingresoFH like '%` + year + `-` + month + `%' group by usuario order by total desc;`

    // query = `select savedBy as usuario, sum(valorPedido) as total, sum(case when venta = 'no' then 1 else 0 end) as no_venta, 
    // sum(case when venta = 'si' then 1 else 0 end) as si_venta,
    // count(distinct(case when venta = 'si' then nombreNegocio end)) as venta_unica from fichacliente inner join gestiondiaria 
    // on gestiondiaria.idCliente = fichacliente.id where ciudadPoblacion like '%` + params.ciudad + `%' and ingresoFH like '%` + year + `-02%' group by usuario order by total desc;`

    Cust.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})


module.exports = router;