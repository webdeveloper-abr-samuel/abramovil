const Sequelize = require('sequelize');
const db = {}
const sequelize = new Sequelize("abrageo", "root", "Abracol2014", { // -/3/88jmu4PXS>97
    host: 'workflow',
    dialect: 'mysql',
    operatorAliases: false,
    dialectOptions: {
        useUTC: false, //for reading from database
        dateStrings: true,
        typeCast: true
    },
    timezone: "-05:00",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db