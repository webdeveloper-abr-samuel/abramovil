var express = require("express");
const Sequelize = require("sequelize");
const fs = require("fs");
const db = require("../database/db.js");

var router = express.Router();

function filterUser(user, nit) {
  return `SELECT gestiondiaria.distribuidor, MAX(ingresoFH) AS fecha FROM gestiondiaria WHERE gestiondiaria.savedBy = '${user}' AND gestiondiaria.nit = '${nit}'`;
}

function filterForm(departamento, distribuidor, fecha, idForm) {
  return `SELECT * FROM formularios
        WHERE formularios.departamento LIKE '%${departamento}%' 
        AND formularios.push = '1' 
        AND formularios.distribuidor LIKE '%${distribuidor}%' 
        AND formularios.fecha LIKE '%${fecha}%'
        AND formularios.id != ${idForm}`;
}

function FilterIdForm(date, user) {
  return `SELECT shopping.idForm FROM shopping
    WHERE shopping.fecha LIKE '%${date}%' AND shopping.usuario = '${user}'`;
}

router.post("/getForm", async (req, res) => {
  const { user, nit, states } = req.body;
  const query = filterUser(user, nit);

  
  var f = new Date();
  const mes = f.getMonth() + 1;
  const mesActual = mes < 10 ? `0${mes}` : mes;
  var date = f.getFullYear() + "-" + mesActual + "-" + f.getDate();

  const queryshopping = FilterIdForm(date, user);

  try {
    let resultGestion = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
    });
    let resultShopping = await db.sequelize.query(queryshopping, {
      type: db.sequelize.QueryTypes.SELECT,
    });

    if (resultGestion != null) {
      let distribuidor = resultGestion.map((el) => el.distribuidor);
      let idForm = resultShopping.map((el) => el.idForm);

      if (idForm != "") {
        Formulario(idForm)
      }else{
        let id = 0;
        Formulario(id)
      }

      async function Formulario(id) {
        const queryform = filterForm(states, distribuidor[0], date,id);
        let resultForm = await db.sequelize.query(queryform, {
          type: db.sequelize.QueryTypes.SELECT,
        });
  
        if (resultForm == "") {
          return res.status(200).json({
            message: "No tienes ninguna encuesta pendiente",
          });
        } else {
          return res.status(200).json({
            resultForm,
          });
        }
      }

    }
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

router.get("/getProducts", (req, res) => {
  db.sequelize
    .query("select * from productos")
    .then((datos) => {
      res.json(datos[0]);
    })
    .catch((err) => {
      console.log(err);
      res.json({
        state: "error",
        data: err,
      });
    });
});

router.post("/guardarRes", (req, res) => {
  var params = req.body;
  query =
    `insert into shopping(idForm, resultado, usuario, fecha, ciudad, departamento, barrio, producto,clasificacion, distribuidor) values('` +
    params.idForm +
    `', '` +
    params.respuesta[0].resultado +
    `', '` +
    params.usr +
    `','` +
    params.respuesta[0].fecha +
    `','` +
    params.respuesta[0].ciudad +
    `','` +
    params.respuesta[0].departamento +
    `','` +
    params.respuesta[0].barrio +
    `','` +
    params.respuesta[0].producto +
    `','` +
    params.respuesta[0].clasificacion +
    `','` +
    params.respuesta[0].distribuidor +
    `');`;
  console.log(query);

  db.sequelize
    .query(query, { type: Sequelize.QueryTypes.INSERT })
    .then((datos) => {
      res.json(datos);
    })
    .catch((err) => {
      console.log(err);
      res.json({
        state: "error",
        data: err,
      });
    });
});

module.exports = router;
