var express = require("express");
var router = express.Router();
var $query = require("../queries/indicadoresSql.js");
const Cust = require('../models/Cust');

router.post("/postSalesMade", async (req, res) => {
  const { asesor } = req.body;
  var sql = $query.postSales;
  try {
    let result = await Cust.sequelize.query(
      sql,
      { replacements: { asesor } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );
    return res.status(200).json({
      data: result[0],
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

router.get("/getAsesores", async (req, res) => {
    var sql = $query.getAsesores;
    try {
      let result = await Cust.sequelize.query(
        sql,
        { type: Cust.sequelize.QueryTypes.SELECT }
      );
      return res.status(200).json({
        data: result,
        message: "Datos obtenidos correctamente",
      });
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
});

router.post("/postOrdersMade", async (req, res) => {
    const { asesor } = req.body;
    var sql = $query.postOrders;
    try {
      let result = await Cust.sequelize.query(
        sql,
        { replacements: { asesor } },
        { type: Cust.sequelize.QueryTypes.SELECT }
      );
      return res.status(200).json({
        data: result[0],
        message: "Datos obtenidos correctamente",
      });
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
});

router.post("/postTotalSale", async (req, res) => {
    const { asesor } = req.body;
    var sql = $query.postTotalSale;
    try {
      let result = await Cust.sequelize.query(
        sql,
        { replacements: { asesor } },
        { type: Cust.sequelize.QueryTypes.SELECT }
      );
      return res.status(200).json({
        data: result[0],
        message: "Datos obtenidos correctamente",
      });
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
});

router.post("/postProduct", async (req, res) => {
    const { asesor } = req.body;
    var sql = $query.postProduct;
    try {
      let result = await Cust.sequelize.query(
        sql,
        { replacements: { asesor } },
        { type: Cust.sequelize.QueryTypes.SELECT }
      );

        let data = [];
        result[0].reduce(function (res, value) {
        if (!res[value.code]) {
            res[value.code] = {
                code: value.code,
                total: 0,
            };
            data.push(res[value.code]);
        }
        res[value.code].total += (value.valor * value.cantidad);
        return res;
        }, {});

        const maxValue = Math.max(...data.map(o => o.total), 0);
        const minValue = Math.min(...data.map(o => o.total));

        const dataMax = data.filter(el => el.total == maxValue);
        const dataMin = data.filter(el => el.total == minValue);


      return res.status(200).json({
        data,
        dataMax,
        dataMin,
        message: "Datos obtenidos correctamente",
      });
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
});

router.post("/postDistri", async (req, res) => {
  const { asesor } = req.body;
  var sql = $query.postDistri;
  try {
    let result = await Cust.sequelize.query(
      sql,
      { replacements: { asesor } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );

      let data = [];
      result[0].reduce(function (res, value) {
      if (!res[value.distribuidor]) {
          res[value.distribuidor] = {
              distribuidor: value.distribuidor,
              valorPedido: 0,
          };
          data.push(res[value.distribuidor]);
      }
      res[value.distribuidor].valorPedido += value.valorPedido;
      return res;
      }, {});

      const maxValue = Math.max(...data.map(o => o.valorPedido), 0);
      const minValue = Math.min(...data.map(o => o.valorPedido));

      const dataMax = data.filter(el => el.valorPedido == maxValue);
      const dataMin = data.filter(el => el.valorPedido == minValue);


    return res.status(200).json({
      data,
      dataMax,
      dataMin,
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

router.post("/postVisit", async (req, res) => {
  const { asesor } = req.body;
  var sql = $query.visited;
  var sql2 = $query.allVisited
  try {
    let result = await Cust.sequelize.query(
      sql,
      { replacements: { asesor } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );

    let visited = await Cust.sequelize.query(
      sql2,
      { replacements: { asesor } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );

    let data = [];
    result[0].reduce(function (res, value) {
    if (!res[value.barrio]) {
        res[value.barrio] = {
            barrio: value.barrio,
            visita: 0,
        };
        data.push(res[value.barrio]);
    }
    res[value.barrio].visita += 1;
    return res;
    }, {});

    return res.status(200).json({
      data,
      visited : visited[0],
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

router.post("/postSalesEffective", async (req, res) => {
  const { asesor, date } = req.body;
  var sql = $query.effective;
  var sql2 = $query.ordersPlaced;
  var fecha = `%${date}%`

  try {
    // venta SI
    let result = await Cust.sequelize.query(
      sql,
      { replacements: { asesor,fecha } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );

    // Sin filtro de Venta
    let allGestion = await Cust.sequelize.query(
      sql2,
      { replacements: { asesor, fecha } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );

    let datosVenta = [];
    result[0].reduce(function (res, value) {
    if (!res[value.name]) {
        res[value.name] = {
            name: value.name,
            mes: 0,
        };
        datosVenta.push(res[value.name]);
    }
    res[value.name].mes += 1;
    return res;
    }, {});

    let datosSinVenta = [];
    allGestion[0].reduce(function (res, value) {
    if (!res[value.name]) {
        res[value.name] = {
            name: value.name,
            total: 0,
        };
        datosSinVenta.push(res[value.name]);
    }
    res[value.name].total += 1;
    return res;
    }, {});

    let datos = []
    datosSinVenta.forEach(element => {
      datosVenta.forEach(el => {
        if(element.name === el.name){
          datos.push({
            name: element.name,
            total: element.total,
            mes: el.mes
          })
        }
      })
    });

    let data = [];
    datos.reduce(function (res, value) {
      if (!res[value.name]) {
        res[value.name] = {
          name: value.name,
          porcentaje: 0,
        };
        data.push(res[value.name]);
      }
      res[value.name].porcentaje = (value.mes / value.total) * 100;
      return res;
    }, {});

    let datas = []
    data.forEach(element => {
      datas.push({
        name: element.name,
        porcentaje: (element.porcentaje).toFixed(2)
      })
    });

    return res.status(200).json({
      datas,
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});


/* --------------------------Abramovil------------------------------ */
router.get("/getDepartamentos", async (req, res) => {
  var sql = $query.getDepartamentos;
  try {
    let result = await Cust.sequelize.query(
      sql,
      { type: Cust.sequelize.QueryTypes.SELECT }
    );
    return res.status(200).json({
      data: result,
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

router.get("/getCiudad", async (req, res) => {
  var sql = $query.getCiudad;
  try {
    let result = await Cust.sequelize.query(
      sql,
      { type: Cust.sequelize.QueryTypes.SELECT }
    );
    return res.status(200).json({
      data: result,
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});

router.post("/coordinates", async (req, res) => {
  const { asesor, date } = req.body;
  var sql = $query.coordinates;
  var fecha = `%${date}%`
  try {
    let result = await Cust.sequelize.query(
      sql,
      { replacements: { asesor,fecha } },
      { type: Cust.sequelize.QueryTypes.SELECT }
    );

    let data = []
    result[0].forEach(element => {
      data.push({
        longitud : parseFloat(element.lng), 
        latitud: parseFloat(element.lat)})
    });

    return res.status(200).json({
      data,
      message: "Datos obtenidos correctamente",
    });
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
});


module.exports = router;
