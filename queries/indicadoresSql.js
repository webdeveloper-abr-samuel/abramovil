var sqlIndicadores = {
    
    postSales : `SELECT  count(Case gestiondiaria.venta when 'Si' then venta end) as SI,
    count(Case gestiondiaria.venta when 'No' then venta end) as NO,
    count(DISTINCT(gestiondiaria.id)) as Cantidad,
    savedBy
    FROM 	gestiondiaria
    WHERE 	gestiondiaria.savedBy = :asesor`,

    getAsesores : `SELECT DISTINCT(savedBy) FROM gestiondiaria`,

    postOrders : `SELECT 
    count(Case idEstado when 1 then id end) as Proceso,
    count(Case idEstado when 2 then id end) as Despachado,
    count(Case idEstado when 3 then id end) as NoDespachado,
    savedBy
    FROM 	gestiondiaria
    WHERE gestiondiaria.savedBy = :asesor`,

    postTotalSale : `SELECT SUM(d.valor * d.cantidad) as cantidad, g.savedBy 
    FROM detalleordens d INNER JOIN gestiondiaria g ON d.idGestion = g.id 
    WHERE g.savedBy = :asesor  AND g.venta = 'Si'`,

    postProduct : `SELECT d.code ,d.valor,d.cantidad  FROM detalleordens d INNER JOIN gestiondiaria g ON d.idGestion = g.id 
    WHERE g.savedBy = :asesor  AND g.venta = 'Si'`,

    postDistri : `SELECT gestiondiaria.valorPedido,  TRIM(LOWER(gestiondiaria.distribuidor)) as distribuidor 
    FROM gestiondiaria 
    WHERE gestiondiaria.savedBy = :asesor AND gestiondiaria.venta = 'Si'`,

    visited : `SELECT TRIM(LOWER(f.barrio)) as barrio
    FROM gestiondiaria g INNER JOIN fichacliente f ON f.id = g.idCliente 
    WHERE g.savedBy = :asesor`,

    allVisited : `SELECT COUNT(TRIM(LOWER(f.barrio))) as barrio
    FROM gestiondiaria g INNER JOIN fichacliente f ON f.id = g.idCliente 
    WHERE g.savedBy = :asesor`,

    effective : `SELECT Date_format(gestiondiaria.ingresoFH,'%M','es_ES') as name
    FROM gestiondiaria 
    WHERE gestiondiaria.savedBy = :asesor AND gestiondiaria.venta = 'Si' AND gestiondiaria.ingresoFH Like :fecha`,

    ordersPlaced : `SELECT Date_format(gestiondiaria.ingresoFH,'%M','es_ES') as name
    FROM gestiondiaria 
    WHERE gestiondiaria.savedBy = :asesor  AND gestiondiaria.ingresoFH  LIKE :fecha`,

    /*--------------------- Abrageo------------------ */

    getDepartamentos : `SELECT DISTINCT(departamento) FROM fichacliente ORDER BY departamento ASC`,

    getCiudad: `SELECT DISTINCT(ciudadPoblacion) as ciudad FROM fichacliente ORDER BY ciudadPoblacion ASC`,

    coordinates : `SELECT fichacliente.lat, fichacliente.lng FROM gestiondiaria,fichacliente WHERE gestiondiaria.idCliente = fichacliente.id AND gestiondiaria.savedBy = :asesor  AND gestiondiaria.ingresoFH  LIKE :fecha`

}

module.exports = sqlIndicadores;