const request = require("supertest");
const app = require("../index");

/*
 *Testing get all consultants endpoint
 */

describe("GET /getAsesores", () => {
  it("respond with json containig a list of all consultants", (done) => {
    request(app)
      .get("/INDICADORES/getAsesores")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});

describe("POST /postSalesMade", () => {
  it("respond with json containig a list of all sales made", (done) => {
    const data = {
      asesor: "KORDONEZ",
    };
    request(app)
      .post("/INDICADORES/postSalesMade")
      .send(data)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done)
      .then((err) => {
        if (err) return done(err);
        done();
      });
  });

  it("respond with code 500 on bad request", (done) => {
    const data = {};
    request(app)
      .post("/INDICADORES/postSalesMade")
      .send(data)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done)
      .then(function(err, res) {
        if (err) return done(err);
        return done();
      });
  });
});
