var express = require('express');
const Cust = require('../models/Cust');
const sequelize = require('sequelize');
const fs = require('fs');
var base64ToImage = require('base64-to-image');
const {Crypt, Descryp} = require('../helpers/hashData');
//Querys
var $query = require('../queries/custSql');

var router = express.Router();

var imagePath = '';

var jsonWrite = function(res, ret) {
    if (typeof ret === 'undefined') {
        res.json({
            code: '1',
            msg: 'operation failed'
        });
    } else {
        res.json(ret);
    }
};

router.delete('/DeleteByID/:Nit', (req, res) => {
    Cust.destroy({
            where: { nit: req.params.Nit }
        })
        .then(delet => {
            if (delet)
                res.json({ 'state': true, 'data': 'Se Elimino con exito' })
            else
                res.json({ 'state': false, 'data': 'La Cliente no existe' })
        })
        .catch(err => {
            console.log(err)
            res.json({ 'state': false, 'data': err })
        })
})


router.post('/UploadPhoto', (req, res) => {
    try {
        var datetime = new Date();

        var folderName = './API/files/Customer/' + datetime.toISOString().slice(0, 10)

        if (!fs.existsSync(folderName))
            fs.mkdirSync(folderName)

        folderName = './API/files/Customer/' + datetime.toISOString().slice(0, 10) + '/' + req.files.file.name.split('.')[0]

        if (!fs.existsSync(folderName))
            fs.mkdirSync(folderName)

        let EDFile = req.files.file
        EDFile.mv(folderName + '/' + datetime.getMinutes() + '_' + datetime.getSeconds() + '_' + req.files.file.name,
            err => {
                if (err)
                    return res.status(500).send({ message: err })
                else {
                    res.json(folderName + '/' + datetime.getMinutes() + '_' + datetime.getSeconds() + '_' + req.files.file.name);
                }
            })
    } catch (err) {
        console.error(err)
    }
})

router.post('/UploadScreen', (req, res) => {
    try {
        var datetime = new Date();

        var folderName = './API/files/Customer/' + datetime.toISOString().slice(0, 10)

        if (!fs.existsSync(folderName)) {
            fs.mkdirSync(folderName);
        }

        folderName = folderName + '/' + req.body.nombreNegocio.replace(/ /g, '')

        if (!fs.existsSync(folderName)) {
            fs.mkdirSync(folderName);
        }

        if (!req.body.canal) {
            req.body.canal = ''
        }

        var base64Str = req.body.foto;
        var optionalObj = { 'fileName': req.body.nombreNegocio.replace(/ /g, ''), 'type': 'jpg' };
        var path = (folderName + '/' + datetime.getHours() + '_')
        var imageInfo = base64ToImage(base64Str, path, optionalObj);
        Cust.sequelize.query('UPDATE fichacliente SET foto="' + req.body.path + '", canal="' + req.body.canal + '" WHERE id= ' + req.body.id, { type: Cust.sequelize.QueryTypes.SELECT }).then(result => {
            res.json(result)
        }).catch(err => {
            res.json(err)
        })

    } catch (err) {
        console.error(err)
    }
})

//DECRYP AND CRYPT
router.post('/GetLisTByName', (req, res) => {
    
    var params = req.body;
    // (departamento like @dep and @dep <> ''  or  @dep = '') and
    //  select * from fichacliente where nombreNegocio like '%%' And '' <> '' or '' = '' Limit 0,30;

    const value = Crypt(params.query);
    Cust.sequelize.query(
            " select * from fichacliente where (nombreNegocio like '%" + value + "%' And '" + value + "' <> '' or '" + value + "' = '')" +
            " union select * from fichacliente where nit like '%" + value + "%' Limit 0,30;", { type: Cust.sequelize.QueryTypes.SELECT })
        .then(task => {
            console.log(Descryp(task[0].nit));
            let data = [];
            
            task.forEach(element => {
                data.push({
                    id: element.id,
                    nombreNegocioConNit: element.nombreNegocio + " - " + element.nit,
                    estrato: element.estrato,
                    telefono: element.telefono,
                    contacto: element.contacto,
                    cargo: element.cargo,
                    correo: element.correo,
                    pais: element.pais,
                    departamento: element.departamento,
                    ciudadPoblacion: element.ciudadPoblacion,
                    territorio: element.territorio,
                    nroComunaZona: element.nroComunaZona,
                    nombreComZon: element.nombreComZon,
                    barrio: element.barrio,
                    clasificacion: element.clasificacion,
                    tipologia: element.tipologia,
                    ordenRuta: element.ordenRuta,
                    comentarios: element.comentarios,
                    direccion: element.direccion,
                    creadoPor: element.creadoPor,
                    creacionFH: element.creacionFH,
                    modificadoPor: element.modificadoPor,
                    modificacionFH: element.modificacionFH,
                    pubExte: element.pubExte,
                    pubInte: element.pubInte,
                    exhibi: element.exhibi,
                    lat: element.lat,
                    lng: element.lng,
                    gooSitioId: element.gooSitioId,
                    foto: element.foto,
                    origenDatos: element.origenDatos,
                    prodAbrasivos: element.prodAbrasivos,
                    lineasS: element.lineasS,
                    promCompr: element.promCompr,
                    UnCorteF: element.UnCorteF,
                    marcas: element.marcas,
                    unFlapD: element.unFlapD,
                    marcasFD: element.marcasFD,
                    promLS: element.promLS,
                    maLijS: element.maLijS,
                    numVerParPer: element.numVerParPer,
                })
            });
            res.send(data)
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            })
        })
})

router.post('/GetByLocation', (req, res) => {
    var params = req.query;
        // (departamento like @dep and @dep <> ''  or  @dep = '') and
        //  select * from fichacliente where nombreNegocio like '%%' And '' <> '' or '' = '' Limit 0,30;
        let latitudeFloat = Descryp(parseFloat(params.lat));
        let latitude = Descryp(params.lat);
        let longitudeFloat = Descryp(parseFloat(params.lng));
        let longitude = Descryp(params.lng);

    Cust.sequelize.query(
            " select * from fichacliente where lat < " + (latitudeFloat + 0.001) + " and lat > " + (latitude - 0.001) + " and lng > " + 
            (longitude - 0.001) + " and lng < " + (longitudeFloat + 0.001) + ";", { type: Cust.sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

//CRYPT
router.post('/GetCustNew', (req, res) => {

    var sql = $query.SaleFormQ.GetCustNew;
    var params = req.body;

    Cust.sequelize.query(sql, {
            replacements: {
                nombreNegocio: Crypt(params.nombreNegocio),
                estrato: Crypt(params.estrato),
                telefono: Crypt(params.telefono),
                contacto: Crypt(params.contacto),
                cargo: Crypt(params.cargo),
                correo: Crypt(params.correo),
                pais: params.pais,
                departamento: params.departamento,
                ciudadPoblacion: params.ciudadPoblacion,
                territorio: params.territorio,
                nroComunaZona: params.nroComunaZona,
                nombreComZon: params.nombreComZon,
                barrio: (params.barrio ? params.barrio : ''),
                clasificacion: Crypt(params.clasificacion),
                tipologia: Crypt(params.tipologia),
                ordenRuta: (params.ordenRuta ? params.ordenRuta : '0'),
                comentarios: params.comentarios,
                direccion: Crypt(params.direccion),
                creadoPor: params.creadoPor,
                pubExte: params.pubExte,
                pubInte: params.pubInte,
                exhibi: params.exhibi,
                nit: Crypt(params.nit),
                taskID: params.taskID,
                form: params.form,
                lat: Crypt(params.lat),
                lng: Crypt(params.lng),
                origenDatos: params.origenDatos,
                prodAbrasivos: Crypt(params.prodAbrasivos),
                lineasS: Crypt(params.lineasS),
                promCompr: Crypt(params.promCompr),
                UnCorteF: Crypt(params.UnCorteF),
                marcas: Crypt(params.marcas),
                unFlapD: Crypt(params.unFlapD),
                marcasFD: Crypt(params.marcasFD),
                promLS: Crypt(params.promLS),
                maLijS: Crypt(params.maLijS),
                numVerParPer: params.numVerParPer
            },
            type: Cust.sequelize.QueryTypes.SELECT
        }).then(CustNew => {
            res.json(CustNew);
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })

})

router.post('/UpdateByID', (req, res) => {

    var sql = $query.SaleFormQ.UpdateByID;

    var params = req.body;

    Cust.sequelize.query(sql, {
            replacements: {
                id: params.id,
                nombreNegocio: Crypt(params.nombreNegocio),
                estrato: Crypt(params.estrato),
                telefono: Crypt(params.telefono),
                contacto: Crypt(params.contacto),
                cargo: Crypt(params.cargo),
                correo: Crypt(params.correo),
                pais: (params.pais ? params.pais : ''),
                departamento: (params.departamento ? params.departamento : ''),
                ciudadPoblacion: (params.ciudadPoblacion ? params.ciudadPoblacion : ''),
                territorio: (params.territorio ? params.territorio : ''),
                nroComunaZona: (params.nroComunaZona ? params.nroComunaZona : ''),
                nombreComZon: params.nombreComZon,
                barrio: (params.barrio ? params.barrio : ''),
                clasificacion: Crypt(params.clasificacion),
                tipologia: Crypt(params.tipologia),
                ordenRuta: (params.ordenRuta ? params.ordenRuta : '0'),
                comentarios: (params.comentarios ? params.comentarios : ""),
                direccion: Crypt(params.direccion),
                modificadoPor: params.modificadoPor,
                pubExte: params.pubExte,
                pubInte: params.pubInte,
                exhibi: params.exhibi,
                nit: Crypt(params.nit),
                taskID: params.taskID,
                form: params.form,
                lat: Crypt(params.lat),
                lng: Crypt(params.lng),
                foto: params.foto,
                origenDatos: params.origenDatos,
                prodAbrasivos: (params.prodAbrasivos ? Crypt(params.prodAbrasivos) : ""),
                lineasS: Crypt(params.lineasS),
                promCompr: Crypt(params.promCompr),
                UnCorteF: Crypt(params.UnCorteF),
                marcas: Crypt(params.marcas),
                unFlapD: Crypt(params.unFlapD),
                marcasFD: Crypt(params.marcasFD),
                promLS: Crypt(params.promLS),
                maLijS: Crypt(params.maLijS),
                numVerParPer: params.numVerParPer,
                pIdRoute: params.pIdRoute
            },
            type: Cust.sequelize.QueryTypes.SELECT
        }).then(Update => {
            res.json(Update);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            })
        })
})

router.post('/UpdateByID_R', (req, res) => {

    var sql = $query.SaleFormQ.UpdateByID_R;

    var params = req.body;

    Cust.sequelize.query(sql, {
            replacements: {
                nombreNegocio: Crypt(params.nombreNegocio),
                estrato: Crypt(params.estrato),
                telefono: Crypt(params.telefono),
                contacto: Crypt(params.contacto),
                cargo: Crypt(params.cargo),
                correo: Crypt(params.correo),
                pais: (params.pais ? params.pais : ''),
                departamento: (params.departamento ? params.departamento : ''),
                ciudadPoblacion: (params.ciudadPoblacion ? params.ciudadPoblacion : ''),
                territorio: (params.territorio ? params.territorio : ''),
                nroComunaZona: (params.nroComunaZona ? params.nroComunaZona : ''),
                nombreComZon: params.nombreComZon,
                barrio: (params.barrio ? params.barrio : ''),
                clasificacion: Crypt(params.clasificacion),
                tipologia: Crypt(params.tipologia),
                ordenRuta: (params.ordenRuta ? params.ordenRuta : '0'),
                comentarios: (params.comentarios ? params.comentarios : ""),
                direccion: Crypt(params.direccion),
                modificadoPor: params.modificadoPor,
                pubExte: params.pubExte,
                pubInte: params.pubInte,
                exhibi: params.exhibi,
                nit: Crypt(params.nit),
                taskID: params.taskID,
                form: params.form,
                lat: Crypt(params.lat),
                lng: Crypt(params.lng),
                foto: params.foto,
                origenDatos: params.origenDatos,
                prodAbrasivos: (params.prodAbrasivos ? Crypt(params.prodAbrasivos) : ""),
                lineasS: (params.lineasS ? Crypt(params.lineasS) : ""),
                promCompr: (params.promCompr ? Crypt(params.promCompr) : ""),
                UnCorteF: (params.UnCorteF ? Crypt(params.UnCorteF) : ""),
                marcas: (params.marcas ? Crypt(params.marcas) : ""),
                unFlapD: (params.unFlapD ? Crypt(params.unFlapD) : ""),
                marcasFD: (params.marcasFD ? Crypt(params.marcasFD) : ""),
                promLS: (params.promLS ? Crypt(params.promLS) : ""),
                maLijS: (params.maLijS ? Crypt(params.maLijS) : ""),
            },
            type: Cust.sequelize.QueryTypes.SELECT
        }).then(Update => {
            res.json(Update);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            })
        })
})

router.post('/UpdateByID_Route', (req, res) => {

    var sql = $query.SaleFormQ.UpdateByID_Route;

    var params = req.body;

    Cust.sequelize.query(sql, {
            replacements: {
                id: params.id,
                nit: Crypt(params.nit),
                nombreNegocio: Crypt(params.nombreNegocio),
                estrato: Crypt(params.estrato),
                telefono: Crypt(params.telefono),
                contacto: Crypt(params.contacto),
                cargo: Crypt(params.cargo),
                correo: Crypt(params.correo),
                pais: (params.pais ? params.pais : ''),
                departamento: (params.departamento ? params.departamento : ''),
                ciudadPoblacion: (params.ciudadPoblacion ? params.ciudadPoblacion : ''),
                territorio: (params.territorio ? params.territorio : ''),
                nroComunaZona: (params.nroComunaZona ? params.nroComunaZona : ''),
                nombreComZon: params.nombreComZon,
                barrio: (params.barrio ? params.barrio : ''),
                clasificacion: Crypt(params.clasificacion),
                tipologia: Crypt(params.tipologia),
                ordenRuta: (params.ordenRuta ? params.ordenRuta : '0'),
                comentarios: (params.comentarios ? params.comentarios : ""),
                direccion: Crypt(params.direccion),
                modificadoPor: params.modificadoPor,
                pubExte: params.pubExte,
                pubInte: params.pubInte,
                exhibi: params.exhibi,
                nit: params.nit,
                taskID: params.taskID,
                form: params.form,
                lat: Crypt(params.lat),
                lng: Crypt(params.lng),
                foto: params.foto,
                origenDatos: params.origenDatos,
                prodAbrasivos: (params.prodAbrasivos ? Crypt(params.prodAbrasivos) : ""),
                lineasS: (params.lineasS ? Crypt(params.lineasS) : ""),
                promCompr: (params.promCompr ? Crypt(params.promCompr) : ""),
                UnCorteF: (params.UnCorteF ? Crypt(params.UnCorteF) : ""),
                marcas: (params.marcas ? Crypt(params.marcas) : ""),
                unFlapD: (params.unFlapD ? Crypt(params.unFlapD) : ""),
                marcasFD: (params.marcasFD ? Crypt(params.marcasFD) : ""),
                promLS: (params.promLS ? Crypt(params.promLS) : ""),
                maLijS: (params.maLijS ? Crypt(params.maLijS) : ""),
                numVerParPer: (params.numVerParPer ? params.numVerParPer : ""),
                pIdRoute: (params.pIdRoute ? params.pIdRoute : -1)
            },
            type: Cust.sequelize.QueryTypes.SELECT
        }).then(Update => {
            res.json(Update);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            })
        })
})


module.exports = router;