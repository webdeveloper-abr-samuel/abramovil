var express = require("express");
var router = express.Router();
const db = require("../database/dbPrueba.js");
const sequelize = require("sequelize");
const fs = require("fs");
const fs_extra = require("fs-extra");
const path = require("path");


router.get("/", async (req, res) => {
  try {
    await Copyfiles();
    const recaudos = await db.sequelize.query("SELECT * FROM recaudos", {
      type: sequelize.QueryTypes.SELECT,
    });

    let data = [];
    recaudos.forEach((el) => {
      data.push({
        name_new: el.numero_en_sistema,
        id_file: el.numero_provisional,
        soporte: el.soporte_pago,
        guia: el.guia,
      });
    });

    let soporte = [];
    let guias = [];

    data.forEach((el) => {
      const values = el.soporte.split("/");
      const fecha = values[4];
      const id_files = values[5];
      if (fecha != null && id_files != null) {
        soporte.push({
          id_files: fecha + "/" + id_files,
          new: fecha + "/" + el.name_new,
        });
      } else {
        const val = el.guia.split("/");
        const fecha = val[4];
        const id_files = val[5];
        guias.push({
          id_files: fecha + "/" + id_files,
          new: fecha + "/" + el.name_new,
        });
      }
    });

    const val = soporte.concat(guias);

    var hash = {};
    result = val.filter(function (current) {
      var exists = !hash[current.id_files];
      hash[current.id_files] = true;
      return exists;
    });

    Rename(result);

    res.status(200).json({
      message: "Todo OK!!",
      result,
    });
  } catch (error) {
    return res.status(500).json({
      mensaje: "Ocurrio un error",
      error: error.message,
    });
  }
});

router.get("/factura", async (req, res) => {
  try {
    const recaudos = await db.sequelize.query("SELECT * FROM recaudos", {
      type: sequelize.QueryTypes.SELECT,
    });

    let data = [];
    recaudos.forEach((el) => {
      data.push({
        name_new: el.numero_en_sistema,
        factura: el.ubicacion_factura,
      });
    });

    let facturas = [];

    data.forEach((el) => {
      const values = el.factura.split("/");
      const fecha = values[4];
      const id_files = values[5];
      if (fecha != null && id_files != null) {
        facturas.push({
          id_files: fecha + "/" + id_files,
          new: fecha + "/" + el.name_new,
        });
      }
    });

    Rename(facturas);

    res.status(200).json({
      message: "Todo OK!!",
      facturas,
    });
  } catch (error) {
    return res.status(500).json({
      mensaje: "Ocurrio un error",
      error: error.message,
    });
  }
});

const Copyfiles = async () => {
  try {
    const dirPath = path.join(__dirname, "/files");
    await fs_extra.copy(`${dirPath}/recaudos`, `${dirPath}/copyRecaudos`);
    console.log("success!");
  } catch (err) {
    console.error(err);
  }
};

const Rename = (data) => {
  const dirPath = path.join(__dirname, "/files/copyRecaudos");
  data.forEach((element) => {
    const route = element.id_files;
    const name = element.new;
    fs.rename(`${dirPath}/${route}`, `${dirPath}/${name}`, (err) => {
      if (err) throw err;
      console.log("Rename complete!");
    });
  });
};


module.exports = router;
