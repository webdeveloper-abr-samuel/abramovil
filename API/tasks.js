var express = require('express');
const Task = require('../models/Task');
const Cust = require('../models/Cust');
const sequelize = require('sequelize');
const Op = sequelize.Op;

var router = express.Router();

router.post('/CloseTask', (req, res) => {
    if (!req.body.usr && !req.body.lat && !req.body.lng && !req.body.taskID) {
        res.status(400)
        res.json({
            state: 'error',
            data: 'Ingrese los campos requeridos'
        })
    } else {
        Task.update({ closeDate: sequelize.literal("NOW()"), clat: req.body.lat, clng: req.body.lng }, { where: { taskID: req.body.taskID, usr: req.body.usr } })
            .then(task => {
                if (task[0]) {
                    res.json({
                        'state': true,
                        'data': 'Se Actualizo con exito'
                    })
                } else {
                    res.json({
                        'state': false,
                        'data': 'No se realizo ningún cambio'
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.json({
                    'state': false,
                    'data': err
                })
            })
    }
})

router.post('/InitTask', (req, res) => {

    if (!req.body.usr && !req.body.lat && !req.body.lng && !req.body.custID) {
        res.status(400)
        res.json({
            state: 'error',
            data: 'Ingrese los campos requeridos'
        })
    } else {
        Task.create(req.body)
            .then(task => {
                res.send({ taskID: task.taskID, custID: task.custID, lastAct: task.lastAct });
            })
            .catch(err => {
                console.log(err)
                res.json({
                    'state': 'error',
                    'data': err
                })
            })
    }

})

router.post('/pendingTask', (req, res) => {
    var params = req.body

    //select taskID, custID from usertask where entryDate is not null and closeDate is null and usr = 'manager' order by entryDate desc limit 1;
    Task.findOne({
            attributes: [
                'taskID', 'custID', 'lastAct'
            ],
            where: {
                entryDate: {
                    [Op.ne]: null
                },
                closeDate: {
                    [Op.eq]: null
                },
                usr: {
                    [Op.eq]: params.usr
                },
            },
            order: [
                ['entryDate', 'DESC']
            ]
        })
        .then((data) => {

            if (data === null)
                res.json("sinPendientes")
            else {
                Cust.findOne({
                        where: {
                            id: {
                                [Op.eq]: data.custID
                            }
                        }
                    })
                    .then((customer) => {
                        res.send({ customer: customer, taskINFO: data })
                    })

            }

        })
        .catch((err) => {
            console.log(err)
            res.send(err)
        })


})
module.exports = router;