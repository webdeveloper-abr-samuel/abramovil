const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
    'fichacliente', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombreNegocio: {
            type: Sequelize.STRING,
        },
        estrato: {
            type: Sequelize.STRING,
        },
        nit: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        telefono: {
            type: Sequelize.STRING,
        },
        contacto: {
            type: Sequelize.STRING,
        },
        cargo: {
            type: Sequelize.STRING,
        },
        correo: {
            type: Sequelize.STRING,
        },
        pais: {
            type: Sequelize.STRING,
        },
        departamento: {
            type: Sequelize.STRING,
        },
        ciudadPoblacion: {
            type: Sequelize.STRING,
        },
        territorio: {
            type: Sequelize.STRING,
        },
        nroComunaZona: {
            type: Sequelize.STRING,
        },
        nombreComZon: {
            type: Sequelize.STRING,
        },
        barrio: {
            type: Sequelize.STRING,
        },
        clasificacion: {
            type: Sequelize.STRING,
        },
        tipologia: {
            type: Sequelize.STRING,
        },
        ordenRuta: {
            type: Sequelize.STRING
        },
        comentarios: {
            type: Sequelize.STRING
        },
        direccion: {
            type: Sequelize.STRING
        },
        creadoPor: {
            type: Sequelize.STRING
        },
        creacionFH: {
            type: Sequelize.DATE
        },
        modificadoPor: {
            type: Sequelize.STRING
        },
        modificacionFH: {
            type: Sequelize.DATE
        },
        pubExte: {
            type: Sequelize.STRING
        },
        pubInte: {
            type: Sequelize.STRING
        },
        exhibi: {
            type: Sequelize.STRING
        },
        lat: {
            type: Sequelize.STRING
        },
        lng: {
            type: Sequelize.STRING
        },
        gooSitioId: {
            type: Sequelize.STRING
        },
        foto: {
            type: Sequelize.STRING
        },
        /* OLD */
        origenDatos: {
            type: Sequelize.STRING
        },
        prodAbrasivos: {
            type: Sequelize.STRING
        },
        lineasS: {
            type: Sequelize.STRING
        },
        promCompr: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        UnCorteF: {
            type: Sequelize.STRING
        },
        marcas: {
            type: Sequelize.STRING
        },
        unFlapD: {
            type: Sequelize.STRING
        },
        marcasFD: {
            type: Sequelize.STRING
        },
        promLS: {
            type: Sequelize.STRING
        },
        maLijS: {
            type: Sequelize.STRING
        },
        numVerParPer: {
            type: Sequelize.STRING,
            length: 200
        },
    }, {
        timestamps: false,
        freezeTableName: true,
    }
)