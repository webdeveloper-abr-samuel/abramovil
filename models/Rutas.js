const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'rutas',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    asesor: {
      type: Sequelize.STRING,
      length: 50,
    },
    estado: {
      type: Sequelize.STRING,
      length: 50,
    },
    fechaAsignacion: {
      type: Sequelize.DATE,
    },
    fechaTerminacion: {
      type: Sequelize.DATE,
    },
    ordenada: {
      type: Sequelize.INTEGER,
    },
    geoRuta: {
      type: Sequelize.STRING,
      length: 1000,
    },
    ordenadaApp: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
)
