const helper = {};
const CryptoJS = require("crypto-js");
const key = CryptoJS.enc.Utf8.parse ("1234123412ABCDEF"); // Dieciséis número hexadecimal como clave
const iv = CryptoJS.enc.Utf8.parse ('ABCDEF1234123412');

helper.Crypt = (value) => {
    let srcs = CryptoJS.enc.Utf8.parse(value);
    let encrypted = CryptoJS.AES.encrypt(srcs, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    return value == '' || value == 'NULL' ? '' : encrypted.ciphertext.toString().toUpperCase()
};
helper.Descryp = (value) => {
    let encryptedHexStr = CryptoJS.enc.Hex.parse(value);
    let srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr);
    let decrypt = CryptoJS.AES.decrypt(srcs, key, { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
    let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
    return value == '' || value == 'NULL' ? '' : decryptedStr.toString();
};

module.exports = helper;