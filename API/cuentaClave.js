var express = require('express');
const Sequelize = require('sequelize');
const fs = require('fs');

var router = express.Router();

const db = {}
/* const sequelize = new Sequelize("abrageo", "root", "Abracol2014", { // -/3/88jmu4PXS>97
    host: 'workflow',
    dialect: 'mysql',
    operatorAliases: false,
    dialectOptions: {
        useUTC: false, //for reading from database
        dateStrings: true,
        typeCast: true
    },
    timezone: "-05:00",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
}) */

const sequelize = new Sequelize('abrageo', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    operatorAliases: false,
    dialectOptions: {
        useUTC: false, //for reading from database
        dateStrings: true,
        typeCast: true
    },
    timezone: "-05:00",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

db.sequelize = sequelize

router.post('/saveCuenta', (req, res) => {
    var params = req.body;
    var query = "REPLACE INTO cliente(nit,nombre,tipo,tamaño,frecuencia,ventas_actuales,ventas_proyectadas,lista_precio,pac,informacion_comercial,productos_compra,productos_codificar,antiguedad_empresa,antiguedad_cliente,areas_empresa,contactos,forma_pago,observaciones,agregado)VALUES ('" + params.nit + "','" + params.nombre + "','" + params.tipo + "','" + params.tamano + "','" + params.frecuencia + "','" + params.ventas_actuales + "','" + params.ventas_proyectadas + "','" + params.lista_precio + "','" + params.pac + "','" + params.informacion_comercial + "','" + params.productos_compra + "','" + params.productos_codifica + "','" + params.antiguedad_empresa + "','" + params.antiguedad_cliente + "','" + params.areas_empresa + "','" + params.contactos + "','" + params.forma_pago + "','" + params.observaciones + "','" + params.usuario + "');";

    sequelize.query(
            query, { type: sequelize.QueryTypes.INSERT })
        .then(task => {
            res.send({ msg: 'Se ha guardado los datos correctamente', opt: 0 })
        })
        .catch(err => {
            console.log(err)
            res.send({ msg: 'No se pudieron guardar los datos', opt: 1 })
        })
})

router.post('/getHojaVida', (req, res) => {
    var params = req.body;
    var query = "select * from cliente where nit='" + params.nit + "';";

    sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.post('/getMatriz', (req, res) => {
    var params = req.body;
    //params.nit = 900085827 //solo para pruebas
    var query = "select nombre, direccion, telefono, email, ciudad, cargo, tipologia, forma_atencion as atencion, observacion as observaciones, razones from matriz where nit='" + params.nit + "';";

    sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.post('/saveMatriz', (req, res) => {
    var params = req.body;
    console.log(params)
        //params.nit = 1 //solo para pruebas

    var queryInsert = "INSERT INTO matriz(nit,opcion,nombre,direccion,telefono,email,ciudad,cargo,tipologia,forma_atencion,observacion, razones) VALUES ('" + params.nit + "','" + 0 + "','" + params.tomaCompra.nombre + "','" + params.tomaCompra.direccion + "','" + params.tomaCompra.telefono + "','" + params.tomaCompra.email + "','" + params.tomaCompra.ciudad + "','" + params.tomaCompra.cargo + "','" + params.tomaCompra.tipologia + "','" + params.tomaCompra.atencion + "','" + params.tomaCompra.observaciones + "','" + params.razon + "'), ('" + params.nit + "','" + 1 + "','" + params.comprador.nombre + "','" + params.comprador.direccion + "','" + params.comprador.telefono + "','" + params.comprador.email + "','" + params.comprador.ciudad + "','" + params.comprador.cargo + "','" + params.comprador.tipologia + "','" + params.comprador.atencion + "','" + params.comprador.observaciones + "',''), ('" + params.nit + "','" + 2 + "','" + params.influenciador.nombre + "','" + params.influenciador.direccion + "','" + params.influenciador.telefono + "','" + params.influenciador.email + "','" + params.influenciador.ciudad + "','" + params.influenciador.cargo + "','" + params.influenciador.tipologia + "','" + params.influenciador.atencion + "','" + params.influenciador.observaciones + "',''), ('" + params.nit + "','" + 3 + "','" + params.apalancador.nombre + "','" + params.apalancador.direccion + "','" + params.apalancador.telefono + "','" + params.apalancador.email + "','" + params.apalancador.ciudad + "','" + params.apalancador.cargo + "','" + params.apalancador.tipologia + "','" + params.apalancador.atencion + "','" + params.apalancador.observaciones + "','');";

    var queryUpdate = "update matriz m join(select 0 as opcion, '" + params.tomaCompra.nombre + "' as new_nombre, '" + params.tomaCompra.direccion + "' as new_direccion, '" + params.tomaCompra.telefono + "' as new_telefono, '" + params.tomaCompra.email + "' as new_email,  '" + params.tomaCompra.ciudad + "' as new_ciudad, '" + params.tomaCompra.cargo + "' as new_cargo,  '" + params.tomaCompra.tipologia + "' as new_tipologia, '" + params.tomaCompra.atencion + "' as new_atencion, '" + params.tomaCompra.observaciones + "' as new_observacion, '" + params.razon + "' as new_razones union all select 1, '" + params.comprador.nombre + "','" + params.comprador.direccion + "','" + params.comprador.telefono + "','" + params.comprador.email + "','" + params.comprador.ciudad + "','" + params.comprador.cargo + "','" + params.comprador.tipologia + "','" + params.comprador.atencion + "', '" + params.comprador.observaciones + "', '' union all    select 2, '" + params.influenciador.nombre + "','" + params.influenciador.direccion + "','" + params.influenciador.telefono + "','" + params.influenciador.email + "','" + params.influenciador.ciudad + "','" + params.influenciador.cargo + "','" + params.influenciador.tipologia + "','" + params.influenciador.atencion + "', '" + params.influenciador.observaciones + "', '' union all  select 3, '" + params.apalancador.nombre + "','" + params.apalancador.direccion + "','" + params.apalancador.telefono + "','" + params.apalancador.email + "','" + params.apalancador.ciudad + "','" + params.apalancador.cargo + "','" + params.apalancador.tipologia + "','" + params.apalancador.atencion + "', '" + params.apalancador.observaciones + "', '') x on x.opcion = m.opcion set nombre=new_nombre,direccion=new_direccion,telefono=new_telefono,email=new_email,ciudad=new_ciudad,cargo=new_cargo,tipologia=new_tipologia,forma_atencion=new_atencion,observacion=new_observacion,razones=new_razones where m.nit = '" + params.nit + "';";


    sequelize.query("SELECT IF ( EXISTS (select * from matriz where nit='" + params.nit + "' and opcion = '0'),1,0) as resultado", { type: sequelize.QueryTypes.SELECT }).then(result => {
            if (result[0].resultado == 1) {
                sequelize.query(
                        queryUpdate)
                    .then(task => {
                        res.send({ msg: 'Se han actualizado los datos correctamente', opt: 0 })
                    })
                    .catch(err => {
                        console.log(err)
                        res.send({ msg: 'No se pudo actualizar los datos', opt: 1 })
                    })
            } else if (result[0].resultado == 0) {
                sequelize.query(
                        queryInsert, { type: sequelize.QueryTypes.INSERT })
                    .then(task => {
                        res.send({ msg: 'Se ha guardado los datos correctamente', opt: 0 })
                    })
                    .catch(err => {
                        console.log(err)
                        res.send({ msg: 'No se pudieron guardar los datos', opt: 1 })
                    })
            }
        })
        .catch(err => {
            res.send(err)
        })
})

router.post('/saveDofa', (req, res) => {
    var params = req.body;
    
    var query = "REPLACE INTO dofa(fortaleza,debilidad,oportunidad,amenazas,nit)VALUES('" + params.fortaleza + "','" + params.debilidad + "','" + params.oportunidad + "','" + params.amenazas + "','" + params.nit + "');";

    sequelize.transaction().then(function (t) {
        return sequelize.query(
            query, 
            { type: sequelize.QueryTypes.INSERT, transaction: t })
                .then(task => {
                    params.roll ? t.rollback() : t.commit()
                    res.send({ msg: 'Se ha guardado los datos correctamente', opt: 0 })
                })
                .catch(function (err) {
                    t.rollback();
                    console.log(err)
                    res.send({ msg: 'No se pudieron guardar los datos', opt: 1 })
                });
      });
})

router.post('/getDofa', (req, res) => {
    var params = req.body;
    var query = "select * from dofa where nit='" + params.nit + "';";

    sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.post('/clienteComercial', (req, res) => {
    params = req.body
    sequelize.query("select * from cliente where agregado like '" + params.asesor + "%';", { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            res.status(200).send({ Datos: datos });
        })
        .catch(err => {
            console.log(err)
            res.status(400).send({ err: err })
        })
})

router.post('/dataUserComercial', (req, res) => {
    var params = req.body;

    //var query = "select * from Erp.UserComp where DcdUserID like '" + params.asesor + "%';";
    var query = "select * from cliente where agregado like '" + params.asesor + "%';";
    sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

module.exports = router;