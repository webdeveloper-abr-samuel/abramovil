const request = require("supertest");
const app = require("../index");

/*
 *Testing get all consultants endpoint
 */

describe("POST /getHojaVida", () => {
  it("respond with json containig a curriculum", (done) => {
    const data = {
      nit: "860009694",
    };
    request(app)
      .post("/CUENTA/getHojaVida")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done)
      .then((err) => {
        if (err) return done(err);
        done();
      });
  });
});

describe("POST /saveDofa", () => {
  it("create test data", (done) => {
    const data = {
      fortaleza: "",
      debilidad: "",
      oportunidad: "",
      amenazas: "",
      nit: "1234",
      roll: "1"
    };
    request(app)
      .post("/CUENTA/saveDofa")
      .send(data)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done)
      .then((err) => {
        if (err) return done(err);
        done();
      });
  });
});
