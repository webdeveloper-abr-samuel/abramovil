var sqlMap = {

    dailyJobQ: {

        GetDailyJob: 'Call GetDailyJob(:idCliente,:venta,:noVenta,:valorPedido,:obsVenta,:nit,:Latitude,:Longitude,:savedBy,:imgRuta,:taskID,:form,:vendedor,:distribuidor,:prodAbrVen);',

        GetDailyJob2: 'Call GetDailyJob2(:idCliente,:venta,:noVenta,:valorPedido,:obsVenta,:nit,:Latitude,:Longitude,:savedBy,:imgRuta,:taskID,:form,:vendedor,:distribuidor,:prodAbrVen, :asesorDistribuidor);',

        ejecucion: "select " +
            "fichacliente.id, venta, noVenta, valorPedido, obsVenta, ingresoFH, vendedor, distribuidor, prodAbrVen, nombreNegocio, " +
            "estrato, gestionDiaria.nit, telefono, contacto, cargo, correo, pais, departamento, ciudadPoblacion, " +
            "territorio, nroComunaZona, nombreComZon, barrio, clasificacion, tipologia, comentarios, direccion, " +
            "pubExte, pubInte, exhibi, lat, lng, prodAbrasivos, lineasS, promCompr, UnCorteF, marcas, unFlapD, marcasFD, promLS, maLijS " +
            "from gestionDiaria " +
            "inner join abrageo.fichacliente " +
            "on fichacliente.id = gestionDiaria.idCliente " +
            "where ingresoFH between :fechaInicial and :fechaFinal " +
            "and savedBy = :guardadoPor order by ingresoFH desc;"

    },

}

module.exports = sqlMap;