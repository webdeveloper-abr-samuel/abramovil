var sqlMap = {

    vtaEfec: `Delimiter ;;
  Set @start = ''
  ;;
  Select Mes,Sum(Total) Total,
        IFNULL(( Select SUM(valorPedido) From dailyjob Where MONTH(ingresoFH) = (MES - 1) And (( savedBy = @start And @start <> '' ) Or ( @start = '' ))  group by MONTH(ingresoFH) ),0) As 'Last',
        Count(Users) Users,
        (Sum(Total) / Count(Users)) As AVG_ADD
  From
  (
      Select MONTH(ingresoFH) As Mes,SUM(valorPedido) As Total,COUNT(savedBy) As Users
      From dailyjob
     Where ( savedBy = @start And @start <> '' ) Or ( @start = '' )
      group by MONTH(ingresoFH),savedBy
  ) As tblTotal
  group by Mes
  Order By Mes
  ;;`,

    gD: "SELECT venta, noVenta, valorPedido, obsVenta, gestiondiaria.nit,Latitude, Longitude, savedBy, ingresoFH, imgRuta, gestiondiaria.taskID, form, vendedor, distribuidor, prodAbrVen, fichacliente.nombreNegocio, direccion, fichacliente.barrio,fichacliente.tipologia, fichacliente.nombreComZon, fichacliente.ciudadPoblacion, telefono, fichacliente.departamento from gestiondiaria LEFT JOIN fichacliente ON gestiondiaria.idCliente = fichacliente.id"

}

module.exports = sqlMap;