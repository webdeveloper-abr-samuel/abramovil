var express = require('express');
const Sequelize = require('sequelize');
var recaudo = express.Router();
var nodemailer = require('nodemailer');
var inLineCss = require('nodemailer-juice');

const fs = require('fs');
var base64ToImage = require('base64-to-image');

const db = {}
const sequelize = new Sequelize("Epicor10", "SA", "Epicor123", {
    host: '10.1.1.31',
    dialect: 'mssql',
    operatorAliases: false,
    timezone: "-05:00",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    }
})

db.sequelize = sequelize

var NotifyEmail = function(correo, params) {
    //informacion para el envio de correo
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'comunicaciones.abracol@abracol.com',
            pass: 'Abr4col2020'
        }
    });

    let efecti = '';
    let cheq = '';
    let cheqdia = '';
    let date = new Date(params.date)
    params.date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

    if (params.payMethod == 'Efectivo') {
        efecti = 'checked';
    }
    if (params.payMethod == 'Cheque') {
        cheq = 'checked';
    }
    if (params.payMethod == 'Cheque Posfechado') {
        cheqdia = 'checked';
    }

    correo = correo + CustomerEmail(params.nit);

    let mailOptions = {
        from: '"Notificacion de pago" comunicaciones.abracol@abracol.com',
        to: correo + ', <g.castro@abracol.com>',
        subject: 'Notificacion de Recaudo',
        html: `<!doctype html>
            <html>

        <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Notificacion de pago - ABRACOL</title>
            <style>
                /All the styling goes here/ img {
                    border: none;
                    -ms-interpolation-mode: bicubic;
                    max-width: 100%;
                }

                body {
                    background-color: #f6f6f6;
                    font-family: sans-serif;
                    -webkit-font-smoothing: antialiased;
                    font-size: 14px;
                    line-height: 1.4;
                    margin: 0;
                    padding: 0;
                    -ms-text-size-adjust: 100%;
                    -webkit-text-size-adjust: 100%;
                }

                table {
                    border-collapse: separate;
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                    width: 100%;
                }

                table td {
                    font-family: sans-serif;
                    font-size: 14px;
                    vertical-align: top;
                }
                /* -------------------------------------
                                    BODY & CONTAINER
                                ------------------------------------- */

                .body {
                    background-color: #f6f6f6;
                    width: 100%;
                }
                /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */

                .container {
                    display: block;
                    margin: 0 auto !important;
                    /* makes it centered */
                    max-width: 750px;
                    padding: 10px;
                    width: 750px;
                }
                /* This should also be a block element, so that it will fill 100% of the .container */

                .content {
                    box-sizing: border-box;
                    display: block;
                    margin: 0 auto;
                    max-width: 750px;
                    padding: 10px;
                }
                /* -------------------------------------
                                    HEADER, FOOTER, MAIN
                                ------------------------------------- */

                .main {
                    background: #ffffff;
                    border-radius: 3px;
                    width: 100%;
                }

                .wrapper {
                    box-sizing: border-box;
                    padding: 20px;
                }

                .content-block {
                    padding-bottom: 10px;
                    padding-top: 10px;
                }

                .footer {
                    clear: both;
                    margin-top: 10px;
                    text-align: center;
                    width: 100%;
                }

                .footer td,
                .footer p,
                .footer span,
                .footer a {
                    color: #999999;
                    font-size: 12px;
                    text-align: center;
                }
                /* -------------------------------------
                                    TYPOGRAPHY
                                ------------------------------------- */

                h1,
                h2,
                h3,
                h4 {
                    color: #000000;
                    font-family: sans-serif;
                    font-weight: 400;
                    line-height: 1.4;
                    margin: 0;
                    margin-bottom: 30px;
                }

                h1 {
                    font-size: 35px;
                    font-weight: 300;
                    text-align: center;
                    text-transform: capitalize;
                }

                p,
                ul,
                ol {
                    font-family: sans-serif;
                    font-size: 14px;
                    font-weight: normal;
                    margin: 0;
                    margin-bottom: 15px;
                }

                p li,
                ul li,
                ol li {
                    list-style-position: inside;
                    margin-left: 5px;
                }

                a {
                    color: #3498db;
                    text-decoration: underline;
                }
                /* -------------------------------------
                                    BUTTONS
                                ------------------------------------- */

                .btn {
                    box-sizing: border-box;
                    width: 100%;
                }

                .btn>tbody>tr>td {
                    padding-bottom: 15px;
                }

                .btn table {
                    width: auto;
                }

                .btn table td {
                    background-color: #ffffff;
                    border-radius: 5px;
                    text-align: center;
                }

                .btn a {
                    background-color: #ffffff;
                    border: solid 1px #3498db;
                    border-radius: 5px;
                    box-sizing: border-box;
                    color: #3498db;
                    cursor: pointer;
                    display: inline-block;
                    font-size: 14px;
                    font-weight: bold;
                    margin: 0;
                    padding: 12px 25px;
                    text-decoration: none;
                    text-transform: capitalize;
                }

                .btn-primary table td {
                    background-color: #3498db;
                }

                .btn-primary a {
                    background-color: #3498db;
                    border-color: #3498db;
                    color: #ffffff;
                }
                /* -------------------------------------
                                    OTHER STYLES THAT MIGHT BE USEFUL
                                ------------------------------------- */

                .last {
                    margin-bottom: 0;
                }

                .first {
                    margin-top: 0;
                }

                .align-center {
                    text-align: center;
                }

                .align-right {
                    text-align: right;
                }

                .align-left {
                    text-align: left;
                }

                .clear {
                    clear: both;
                }

                .mt0 {
                    margin-top: 0;
                }

                .mb0 {
                    margin-bottom: 0;
                }

                .preheader {
                    color: transparent;
                    display: none;
                    height: 0;
                    max-height: 0;
                    max-width: 0;
                    opacity: 0;
                    overflow: hidden;
                    mso-hide: all;
                    visibility: hidden;
                    width: 0;
                }

                .powered-by a {
                    text-decoration: none;
                }

                hr {
                    border: 0;
                    border-bottom: 1px solid #f6f6f6;
                    margin: 20px 0;
                }
                /* -------------------------------------
                                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                                ------------------------------------- */

                @media only screen and (max-width: 620px) {
                    table[class=body] h1 {
                        font-size: 28px !important;
                        margin-bottom: 10px !important;
                    }
                    table[class=body] p,
                    table[class=body] ul,
                    table[class=body] ol,
                    table[class=body] td,
                    table[class=body] span,
                    table[class=body] a {
                        font-size: 16px !important;
                    }
                    table[class=body] .wrapper,
                    table[class=body] .article {
                        padding: 10px !important;
                    }
                    table[class=body] .content {
                        padding: 0 !important;
                    }
                    table[class=body] .container {
                        padding: 0 !important;
                        width: 100% !important;
                    }
                    table[class=body] .main {
                        border-left-width: 0 !important;
                        border-radius: 0 !important;
                        border-right-width: 0 !important;
                    }
                    table[class=body] .btn table {
                        width: 100% !important;
                    }
                    table[class=body] .btn a {
                        width: 100% !important;
                    }
                    table[class=body] .img-responsive {
                        height: auto !important;
                        max-width: 100% !important;
                        width: auto !important;
                    }
                }
                /* -------------------------------------
                                    PRESERVE THESE STYLES IN THE HEAD
                                ------------------------------------- */

                @media all {
                    .ExternalClass {
                        width: 100%;
                    }
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {
                        line-height: 100%;
                    }
                    .apple-link a {
                        color: inherit !important;
                        font-family: inherit !important;
                        font-size: inherit !important;
                        font-weight: inherit !important;
                        line-height: inherit !important;
                        text-decoration: none !important;
                    }
                    #MessageViewBody a {
                        color: inherit;
                        text-decoration: none;
                        font-size: inherit;
                        font-family: inherit;
                        font-weight: inherit;
                        line-height: inherit;
                    }
                    .btn-primary table td:hover {
                        background-color: #34495e !important;
                    }
                    .btn-primary a:hover {
                        background-color: #34495e !important;
                        border-color: #34495e !important;
                    }
                }
            </style>
        </head>

        <body class="">
            <span class="preheader">Notificacion de pago.</span>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
                <tr>
                    <td>&nbsp;</td>
                    <td class="container">
                        <div class="content">

                            <!-- START CENTERED WHITE CONTAINER -->
                            <table role="presentation" class="main">

                                <!-- START MAIN CONTENT AREA -->
                                <tr>
                                    <td class="wrapper">
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div style="position: absolute; top: 0; left: 0; width: 100%;height: 100%;z-index: 2;filter: alpha(opacity = 100);"></div>
                                                    <div style='width: 400px;'>
                                                        <p><strong>Abracol S.A</strong></p>
                                                        <p><strong>Nit: 890.911.327-1</strong></p>
                                                        <p>KM 20 Autopista Norte</p>
                                                        <p>Girardota, Antioquia.</p>
                                                        <h2 style='background-color: #fff24d; text-align: center;'><strong>Notificacion de pago </strong></h2>
                                                        <p>Razon social: ` + params.company + `</p>
                                                        <p>Nit: ` + params.nit + `</p>
                                                        <p>Esta es la notificación de pago recibido el cual será consignado a nuestra cuenta bancaria. Al momento de hacerse efectiva la consignación será enviado a su correo electrónico el comprobante de pago. </p>
                                                        <table>
                                                            <tr style='width: 100%; text-align: left;'>
                                                                <th style='width: 50%; text-align: left;'><strong>EFECTIVO</strong></th>
                                                                <th style='text-align: center;'> <input type="checkbox" style='width: 50%; color:#fff24d;' ` + efecti + `></button>
                                                                </th>
                                                            </tr>
                                                            <tr style='width: 100%; text-align: left;'>
                                                                <th style='width: 50%; text-align: left;'><strong>CHEQUE AL DIA</strong></th>
                                                                <th style='text-align: center;'> <input type="checkbox" style='width: 50%; color:#fff24d;' ` + cheq + `></button>
                                                                </th>
                                                            </tr>
                                                            <tr style='width: 100%; text-align: left;'>
                                                                <th style='width: 50%; text-align: left;'><strong>CHEQUE POSFECHADO</strong></th>
                                                                <th style='text-align: center;'> <input type="checkbox" style='width: 50%; color:#fff24d;' ` + cheqdia + `></button>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                        <table style="border-collapse: collapse !important;border: 1px solid black; margin-top: 20px;margin-bottom: 20px;">
                                                            <tr style="border: 1px solid black;">
                                                                <th style="border: 1px solid black;">
                                                                    VALOR:
                                                                </th>
                                                                <td style="border: 1px solid black;">
                                                                    $ ` + params.payAmount.toLocaleString() + `
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid black;">
                                                                <th style="border: 1px solid black;">
                                                                    FECHA DE CONSIGNACION:
                                                                </th>
                                                                <td style="border: 1px solid black;">
                                                                    ` + params.date + `
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <h2 style='background-color: #fff24d; text-align: center;'><strong>Preguntas y/o inquietudes</strong></h2>
                                                        <p>E-mail: ` + correo + `
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <!-- END MAIN CONTENT AREA -->
                            </table>
                            <!-- END CENTERED WHITE CONTAINER -->


                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </body>

        </html>`

    };

    transporter.use('compile', inLineCss());

    transporter.sendMail(mailOptions);

}

var CustomerEmail = function(nit) {
    sequelize.query(
            "Select EMailAddress From Customer C where C.CustID = '" + nit + "' ;", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            let data = []

            return task;
        })
}

var uploadPhoto = function(imagen, company, nombre) {

    try {
        let date = new Date()
        let today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        var folderName = './API/files/Recaudo/' + today
        if (!fs.existsSync(folderName))
            fs.mkdirSync(folderName)

        var folderName = './API/files/Recaudo/' + today + '/' + company + '/'

        if (!fs.existsSync(folderName))
            fs.mkdirSync(folderName)
        var matches = imagen.replace(/^data:image\/jpeg;base64,/, "");

        var base64Str = imagen
        var optionalObj = { 'fileName': nombre, 'type': 'jpg' }
        var path = folderName
        var imageInfo = base64ToImage(base64Str, path, optionalObj)
            //console.log('/API/files/Recaudo/' + today + '/' + company + '/' + nombre + '.jpg')
        return ('/API/files/Recaudo/' + today + '/' + company + '/' + nombre + '.jpg')
    } catch (err) {
        console.log(err)
    }
}

recaudo.post('/GetBill', (req, res) => {
    var params = req.body;
        // (departamento like @dep and @dep <> ''  or  @dep = '') and
        //  select * from fichacliente where nombreNegocio like '%%' And '' <> '' or '' = '' Limit 0,30;
    sequelize.query(
            "Select C.CustID Nit ,C.Name Cliente,IH.* From Invchead IH inner join Customer C on IH.CustNum = C.CustNum where (C.CustID = '" + params.nit + "' or C.Name LIKE '%" + params.nit + "%') and IH.OpenInvoice = 1 and (IH.InvoiceBal > 0 or IH.DocInvoiceBal > 0) order by IH.InvoiceDate asc;", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

recaudo.get('/count', (req, res) => {

    sequelize.query(
            "select TOP 10 cast(Key1 as int) as Recibo from Ice.UD38 WHERE Key2='RECIBO-MOVIL' order by Key1+0 desc;", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

recaudo.post('/GetPendienteBill', (req, res) => {
    var params = req.body;
    sequelize.query(
            "select UD.*, IH.DocInvoiceAmt,IH.LegalNumber, IH.InvoiceDate, UD.ShortChar03 as DocInvoiceBal from Ice.UD38 UD inner join InvcHead IH on UD.Key3 = IH.LegalNumber where Key1 = 'RECAUDO-MOVIL' and Key2 = '" + params.id + "';", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

recaudo.post('/GetPendientes', (req, res) => {
    var params = req.body;

    sequelize.query(
            "select * from Ice.UD38 where Character10 = '" + params.usuario + "' and Key2 = 'RECIBO-MOVIL' and CheckBox01 = 0;", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            let data = []

            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

recaudo.post('/updateRecaudo', (req, res) => {
    //console.log('entra');
    var params = req.body;
    let date = new Date()
    let dateRestriction = new Date(params.date)
    let today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    // today = '2021-02-28'
    let daysD;
    let query = '';
    if (dateRestriction.getMonth() == date.getMonth()) {
        today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    } else {
        d = new Date();
        d.setFullYear(date.getFullYear(), date.getMonth(), 0);
        today = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
    }
    for (let i = 0; i < params.legalNumber.length; i++) {
        daysD = 0
        if (params.billDiscount[i] == '2') {
            daysD = '30'
        }

        if (params.billDiscount[i] == '2.5') {
            daysD = '15'
        }

        if (params.billDiscount[i] == '3.5') {
            daysD = '8'
        }


        if (params.legalNumber[i] == '0') {
            params.guias[i] = 0;
        }
        //console.log(params);

        if (typeof params['guias'] != 'undefined') {
            if (params.guias[i] != '0' || typeof params.guias[i] != 'string' || params.guias[i] != 'undefined') {
                params.guias[i] = uploadPhoto(params.guias[i], params.recibo, 'Soporte_de_Entrega_' + params.legalNumber[i]);
            } else {
                params.guias[i] = 0;
            }
        } else {
            params.push({ 'guias': 0 });
        }

        if ((i + 1) == params.legalNumber.length) {
            query = query + "('ABRACOL','RECAUDO-MOVIL','" + params.recibo + "','" + params.legalNumber[i] + "','" + params.company + "','" + params.total + "','" + params.billDiscount[i] + "','" + daysD + "','" + params.bills[i] + "','" + params.billDate[i] + "','" + params.NCLegalN[i] + "','" + params.NCValue[i] + "','" + params.guias[i] + "');"
        } else {
            query = query + "('ABRACOL','RECAUDO-MOVIL','" + params.recibo + "','" + params.legalNumber[i] + "','" + params.company + "','" + params.total + "','" + params.billDiscount[i] + "','" + daysD + "','" + params.bills[i] + "','" + params.billDate[i] + "','" + params.NCLegalN[i] + "','" + params.NCValue[i] + "','" + params.guias[i] + "'),"
        }

    }

    if (params.soporPago && params.soporPago.includes('/API/') == false) {
        params.soporPago = uploadPhoto(params.soporPago, params.recibo, 'Soporte_de_Pago');
    }
    if (params.soporEntre && params.soporEntre.includes('/API/') == false) {
        params.soporEntre = uploadPhoto(params.soporEntre, params.recibo, 'Soporte_de_Entrega');
    } else {
        params.soporEntre = '';
    }
    if (params.soporAuEsp && params.soporAuEsp.includes('/API/') == false) {
        params.soporAuEsp = uploadPhoto(params.soporAuEsp, params.recibo, 'Soporte_de_Especial');
    }
    // if ((dateRestriction.getMonth() + 1) == 3) {
    let rechazo = JSON.stringify(params.rechazo);

    sequelize.query(`begin TRANSACTION
            begin try 
                delete from Ice.UD38 where Key1 = '` + params.recibo + `' or Key2 = '` + params.recibo + `';
                insert into [Ice].[UD38] (Company, Key1, Key2, Key3, Key4, Key5, ShortChar04, ShortChar05, ShortChar03, Date03, ShortChar01, ShortChar02,Character05) Values` + query + ` insert into [Ice].[UD38] (Company, Key2, Key1, Key3, Key4, Key5, Character01, Character02, Character03,Character04,Character05,Character06, Character09,Character10, Date01, Date02, ShortChar01, ShortChar02, ShortChar03) values('ABRACOL','RECIBO-MOVIL','` + params.recibo + `','` + params.nit + `','` + params.company + `','` + params.total + `','` + params.observacion + `','` + params.payMethod + `','` + params.bank + `','` + params.soporPago + `','` + params.soporEntre + `','` + params.soporAuEsp + `','` + rechazo + `','` + params.usuario + `','` + params.date + `','` + today + `','` + params.discount + `','` + params.money + `','` + params.payAmount + `');
                select 1 as mensaje;
                COMMIT TRANSACTION;
            end try
            begin catch
                select 2 as mensaje;
                select ERROR_MESSAGE() as error;
                ROLLBACK TRANSACTION;
            end catch`).then(task => {
            if (task[0][0].mensaje == 1) {
                // if (params.payMethod == 'Efectivo' || params.payMethod == 'Cheque' || params.payMethod == 'Cheque Posfechado') {
                //     sequelize.query("select EMailAddress as Email from Ice.SysUserFile where UserID = '" + params.usuario + "';")
                //         .then(email => {
                //             NotifyEmail(email[0][0].Email, params)
                //         })
                // }
                res.send({ msg: 'Se ha actualizado la informacion correctamente', stat: 0 })
            } else {
                res.send({ msg: task[0][1].error, stat: 1 })
            }
        })
        .catch(err => {
            res.send(err)
        })
        // } else {
        //     res.send({ msg: 'fecha del recibo no es de febrero, por cierre de cartera, no se permitira la realizacion de estos recibos hasta nueva orden', stat: 0 })
        // }
})


recaudo.post('/GetNC', (req, res) => {
    var params = req.body;
    sequelize.query(
            "Select C.CustID Nit ,C.Name Cliente,IH.* From Invchead IH inner join Customer C on IH.CustNum = C.CustNum where C.CustID = '" + params.nit + "' and IH.OpenInvoice = 1 and (LEFT(LegalNumber,2) = 'UR' or LEFT(LegalNumber,2) = 'NC');", { type: sequelize.QueryTypes.SELECT })
        .then(task => {

            res.send(task)
        })
        .catch(err => {
            res.send(err)
        })
})

recaudo.post('/anticipo', (req, res) => {
    var params = req.body;
    sequelize.query(
            "Select C.CustID Nit ,C.Name Cliente From Customer C where C.CustID = '" + params.nit + "';", { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            if (task.length != 0) {
                res.send(task)
            } else {
                res.send({ msg: 'No se encontro el nit del cliente' })
            }

        })
        .catch(err => {
            res.send(err)
        })
})

recaudo.post('/saveRecaudo', (req, res) => {
    var params = req.body;


    // if (typeof params.version == 'undefined' || params.version != process.env.VERSION) {
    //     res.send({ stat: 0, msg: 'su version del abramovil esta desactualizada, por favor actualicela a la ultima version para evitar inconvenientes' })
    // } else {
    let date = new Date()
    let dateRestriction = new Date(params.date)
    let today;
    if (dateRestriction.getMonth() == date.getMonth()) {
        today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    } else {
        d = new Date();
        d.setFullYear(date.getFullYear(), date.getMonth(), 0);
        today = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
    }


    let daysD;
    let query = '';
    for (let i = 0; i < params.legalNumber.length; i++) {
        daysD = 0
        if (params.billDiscount[i] == '2') {
            daysD = '30'
        }
        if (params.billDiscount[i] == '2.5') {
            daysD = '15'
        }
        if (params.billDiscount[i] == '3.5') {
            daysD = '8'
        }

        //console.log(typeof params['guias']);

        if (typeof params['guias'] != 'undefined') {
            if (params.guias[i] != '0') {
                params.guias[i] = uploadPhoto(params.guias[i], params.recibo, 'Soporte_de_Entrega_' + params.legalNumber[i]);
            } else {
                params.guias[i] = 0;
            }
        }


        if ((i + 1) == params.legalNumber.length) {
            query = query + "('ABRACOL','RECAUDO-MOVIL','" + params.recibo + "','" + params.legalNumber[i] + "','" + params.company + "','" + params.total + "','" + params.billDiscount[i] + "','" + daysD + "','" + params.bills[i] + "','" + params.billDate[i] + "','" + params.NCLegalN[i] + "','" + params.NCValue[i] + "','" + params.guias[i] + "');"
        } else {
            query = query + "('ABRACOL','RECAUDO-MOVIL','" + params.recibo + "','" + params.legalNumber[i] + "','" + params.company + "','" + params.total + "','" + params.billDiscount[i] + "','" + daysD + "','" + params.bills[i] + "','" + params.billDate[i] + "','" + params.NCLegalN[i] + "','" + params.NCValue[i] + "','" + params.guias[i] + "'),"
        }

    }
    if (params.soporPago) {
        params.soporPago = uploadPhoto(params.soporPago, params.recibo, 'Soporte_de_Pago');
    }
    if (params.soporEntre) {
        params.soporEntre = uploadPhoto(params.soporEntre, params.recibo, 'Soporte_de_Entrega');
    }
    if (params.soporAuEsp) {
        params.soporAuEsp = uploadPhoto(params.soporAuEsp, params.recibo, 'Soporte_de_Especial');
    }

    date = new Date(params.date);
    let now = new Date();
    let compareDay;
    let compareMonth;
    let compareYear;
    let compare;
    date.setDate(date.getDate());
    compareDay = date.getDate() - now.getDate()
    compareMonth = date.getMonth() - now.getMonth()
    compareYear = date.getFullYear() - now.getFullYear()
    compare = compareDay + (compareMonth * 30) + (compareYear * 361)

    // if (params.usuario == 'thernandez') {
    if (dateRestriction.getMonth() <= now.getMonth()) { //solo para restriccion de cartera por mes
        if ((params.payMethod == 'Efectivo' || params.payMethod == 'Cheque' || params.payMethod == 'Cheque Posfechado') && compare > 2) {
            sequelize.query("select EMailAddress as Email from Ice.SysUserFile where UserID = '" + params.usuario + "';")
                .then(email => {
                    NotifyEmail(email[0][0].Email, params)
                    res.send({ stat: 1, msg: 'Se ha notificado al cliente del efectivo que se ha recibido' })
                })
        } else {
            sequelize.query(
                    "IF NOT EXISTS (select Key1 from [Ice].[UD38] where Key2='" + params.recibo + "')insert into [Ice].[UD38] (Company, Key1, Key2, Key3, Key4, Key5, ShortChar04, ShortChar05, ShortChar03, Date03, ShortChar01, ShortChar02,Character05) Values" + query + "IF NOT EXISTS (select Key1 from [Ice].[UD38] where Key1='" + params.recibo + "') insert into [Ice].[UD38] (Company, Key2, Key1, Key3, Key4, Key5, Character01, Character02, Character03,Character04,Character05,Character06, Character10, Date01, Date02, ShortChar01, ShortChar02, ShortChar03) values('ABRACOL','RECIBO-MOVIL','" + params.recibo + "','" + params.nit + "','" + params.company + "','" + params.total + "','" + params.observacion + "','" + params.payMethod + "','" + params.bank + "','" + params.soporPago + "','" + params.soporEntre + "','" + params.soporAuEsp + "','" + params.usuario + "','" + params.date + "','" + today + "','" + params.discount + "','" + params.money + "','" + params.payAmount + "') else select 0 as mensaje;", { type: sequelize.QueryTypes.SELECT })
                .then(task => {
                    console.log(task.length)
                    if (task.length == 0) {
                        res.send({ stat: 1, msg: 'Su numero de recibo es: ' + params.recibo + '. se ha guardado la informacion correctamente' })
                    } else {
                        res.send({ stat: 0, msg: 'El numero de recibo que se estaba generando ha sido tomado, por favor trate de guardar la informacion nuevamente' })
                    }
                })
                .catch(err => {
                    res.send(err)
                })
        }
    } else {
        res.send({ stat: 0, msg: 'no se permitira la realizacion de nuevos recibos hasta nueva orden' })
    }
    // } else {
    //     res.send({ stat: 0, msg: 'no se permitira la realizacion de nuevos recibos hasta nueva orden' })
    // }
    // }

})


module.exports = recaudo;