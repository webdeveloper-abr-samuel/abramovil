var express = require('express');
const Visitas = require('../models/Visitas')
const Rutas = require('../models/Rutas')
const sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const config = require('./config/config');
//Querys
var $query = require('../queries/abraGeoSql.js');

var router = express.Router();

const dbEpicor = require('../database/dbEpicor.js')

function jwtRegUser(user) {
    const ONE_HOUR = 60 * 60;
    return jwt.sign(JSON.parse(JSON.stringify(user)), config.authentication.jwtSecret, {
        expiresIn: ONE_HOUR
    });
}


router.post('/getRoutes', (req, res) => {

    var params = req.body;

    var d = new Date()
    var month = (d.getMonth() + 1)
    var day = d.getDate()
    var year = d.getFullYear()

    if (month < 10) {
        month = '0' + month
    }

    date = year + '-' + month + '-' + day

    var query = "select " +
        "visitas.id, visitas.idRuta, visitas.orden, visitas.estado, rutas.geoRutas," +
        "fichacliente.nombreNegocio,fichacliente.estrato,fichacliente.nit,fichacliente.telefono," +
        "fichacliente.contacto,fichacliente.cargo,fichacliente.correo,fichacliente.pais," +
        "fichacliente.departamento,fichacliente.ciudadPoblacion,fichacliente.territorio," +
        "fichacliente.nroComunaZona,fichacliente.nombreComZon,fichacliente.barrio," +
        "fichacliente.clasificacion,fichacliente.tipologia,fichacliente.ordenRuta," +
        "fichacliente.comentarios,fichacliente.direccion,fichacliente.pubExte," +
        "fichacliente.pubInte,fichacliente.exhibi,fichacliente.prodAbrasivos," +
        "fichacliente.lineasS,fichacliente.promCompr,fichacliente.UnCorteF," +
        "fichacliente.marcas,fichacliente.unFlapD,fichacliente.marcasFD," +
        "fichacliente.promLS,fichacliente.maLijS,fichacliente.lat,fichacliente.lng " +
        "from abrageo.visitas " +
        "inner join abrageo.fichacliente " +
        "on fichacliente.id = visitas.idCliente " +
        "inner join rutas " +
        "on rutas.id = visitas.idRuta " +
        "where rutas.asesor like '%" + params.asesor + "%' and rutas.fechaAsignacion = '" + date + "';"

    Visitas.sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.send(err);
        });

});

router.post('/getRoutesApp', (req, res) => {

    var params = req.body;

    var query = "select " +
        "visitas.id, visitas.idRuta, visitas.nit, visitas.orden, visitas.estado, visitas.nit," +
        "fichacliente.nombreNegocio,fichacliente.estrato,fichacliente.nit,fichacliente.telefono," +
        "fichacliente.contacto,fichacliente.cargo,fichacliente.correo,fichacliente.pais," +
        "fichacliente.departamento,fichacliente.ciudadPoblacion,fichacliente.territorio," +
        "fichacliente.nroComunaZona,fichacliente.nombreComZon,fichacliente.barrio," +
        "fichacliente.clasificacion,fichacliente.tipologia,fichacliente.ordenRuta," +
        "fichacliente.comentarios,fichacliente.direccion,fichacliente.pubExte," +
        "fichacliente.pubInte,fichacliente.exhibi,fichacliente.prodAbrasivos," +
        "fichacliente.lineasS,fichacliente.promCompr,fichacliente.UnCorteF," +
        "fichacliente.marcas,fichacliente.unFlapD,fichacliente.marcasFD," +
        "fichacliente.promLS,fichacliente.maLijS,fichacliente.lat,fichacliente.lng, " +
        "(select ordenadaApp from rutas where fechaAsignacion = '" + params.fechaAsignacion + "') as 'ordenadaApp' " +
        "from abrageo.visitas " +
        "inner join abrageo.fichacliente " +
        "on fichacliente.nit = visitas.nit " +
        "inner join abrageo.Rutas " +
        "on rutas.id = visitas.idRuta " +
        "where idRuta =(select id from rutas where fechaAsignacion = '" + params.fechaAsignacion + "');"

    Visitas.sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.send(data);
        });

});

router.post('/getVisits', (req, res) => {

    var query = "select " +
        "fichacliente.id,fichacliente.nombreNegocio,fichacliente.estrato,fichacliente.nit,fichacliente.telefono," +
        "fichacliente.contacto,fichacliente.cargo,fichacliente.correo,fichacliente.pais," +
        "fichacliente.departamento,fichacliente.ciudadPoblacion,fichacliente.territorio," +
        "fichacliente.nroComunaZona,fichacliente.nombreComZon,fichacliente.barrio," +
        "fichacliente.clasificacion,fichacliente.tipologia,fichacliente.ordenRuta," +
        "fichacliente.comentarios,fichacliente.direccion,fichacliente.pubExte," +
        "fichacliente.pubInte,fichacliente.exhibi,fichacliente.prodAbrasivos," +
        "fichacliente.lineasS,fichacliente.promCompr,fichacliente.UnCorteF," +
        "fichacliente.marcas,fichacliente.unFlapD,fichacliente.marcasFD," +
        "fichacliente.promLS,fichacliente.maLijS,fichacliente.lat,fichacliente.lng, fichacliente.numVerParPer, visitas.estado, visitas.orden " +
        "from abrageo.visitas " +
        "inner join abrageo.fichacliente " +
        "on fichacliente.id = visitas.idCliente " +
        "inner join abrageo.Rutas " +
        "on rutas.id = visitas.idRuta " +
        "where rutas.id = " + req.body.idRuta + ";"

    Visitas.sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.send(data);
        });

});

router.post('/getVisitsComercial', (req, res) => {

    params = req.body;


    //console.log(params)


    var query = "select * from Customer where ResaleID = '" + params.idRuta + "';"
    dbEpicor.sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.send(data);
        });
});

router.post('/getVisitsComercial2', async(req, res) => {

    params = req.body;

    date = [];

    // console.log(params.length)

    for (i = 0; i < params.length; i++) {

        var query = "select * from Customer where ResaleID = '" + params[i].idCliente + "';"
        try {
            data = await dbEpicor.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })


            date.push(data[0])
            if (date.length == (params.length)) {
                res.send(date);
            }
        } catch {
            console.log(err);
            res.send(data);
        };

    }


});


router.post('/ordenarVisitas', (req, res) => {

    var params = req.body;

    console.log(params)

    Visitas.update({ orden: params.orden }, { where: { id: params.id, idRuta: params.idRuta } })
        .then((data) => {
            res.send(data)
        })
        .catch((error) => {
            console.log(error);
            res.send(error);
        })


});


router.post('/sendGeoRoute', (req, res) => {

    console.log(req.body)

    var params = req.body;

    Rutas.update({ ordenada: 1, geoRuta: params.geoRuta }, { where: { id: params.id } })
        .then((data) => {
            res.send(data)
        })
        .catch((error) => {
            console.log(error);
            res.send(error);
        })


});

router.post('/isOrdered', (req, res) => {

    Rutas.findOne({
            where: { id: req.body.idRuta },
            attributes: ['ordenada', 'geoRuta']
        })
        .then((data) => {
            res.send(data)
        })
        .catch((error) => {
            console.log(error);
            res.send(error);
        })


});


router.post('/isOrderedApp', (req, res) => {


    var paramas = req.body

    paramas.fechaAsignacion = '2021-04-03'

    var query = "select visitas.id, visitas.idCliente, visitas.idRuta, visitas.orden, visitas.estado, rutas.fechaAsignacion from visitas inner join rutas on visitas.idRuta = rutas.id where fechaAsignacion = '" + paramas.fechaAsignacion + "' and asesor like '%" + paramas.asesor + "%';";


    Rutas.sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.send(data);
        });

});

router.post('/guardarVisitaComercial', (req, res) => {


    var params = req.body

    var query = "UPDATE abrageo.visitas SET estado = 'visitada', fechaModificacion = '" + params.fechaModificacion + "', observacion = '" + params.observacion + "', areaAtendida = '" + params.areaAtendida + "', tema = '" + params.tema + "', informacion = '" + params.informacion + "', compromiso = '" + params.compromiso + "' WHERE id = '" + params.id + "';";


    Rutas.sequelize.query(query, { type: sequelize.QueryTypes.UPDATE }).then(data => {
            res.send({ ok: 1 });
        })
        .catch(err => {
            console.log(err);
            res.send({ ok: 0 });
        });

});



router.post('/sendGeoRouteApp', (req, res) => {

    var params = req.body;

    Rutas.update({ ordenadaApp: 1, geoRuta: params.geoRuta }, { where: { id: params.id } })
        .then((data) => {
            res.send(data)
        })
        .catch((error) => {
            console.log(error);
            res.send(error);
        })


});


router.post('/actualizarEstado', (req, res) => {

    var params = req.body;

    Visitas.update({ estado: 'visitada', fechaModificacion: sequelize.fn('NOW'), observacion: params.observacion }, { where: { idCliente: params.idCliente } })
        .then((estadoActualizado) => {

            //res.send(estadoActualizado)

            Visitas.findAll({
                    attributes: {
                        include: [
                            [sequelize.fn('COUNT', sequelize.col('estado')), 'cuentaEstado']
                        ],
                        exclude: ['id', 'idCliente', 'orden', 'fechaModificacion', 'observacion', 'idRuta', 'estado']
                    },
                    where: { idRuta: params.idRuta, estado: 'pendiente' }
                })
                .then((data) => {

                    console.log("cuentaEstado:")
                    console.log(data[0].dataValues.cuentaEstado)

                    if (data[0].dataValues.cuentaEstado === 0) {
                        Rutas.update({ estado: 'terminada', fechaTerminacion: sequelize.fn('NOW') }, { where: { id: params.idRuta } })
                            .then((r) => { res.send(estadoActualizado) })
                            .catch((e) => {
                                console.log(e);
                                res.send(e)
                            })
                    } else
                        res.send(estadoActualizado)

                })
                .catch((er) => {
                    console.log(er);
                    res.send(er);
                })

        })
        .catch((error) => {
            console.log(error);
            res.send(error);
        })


});


router.post('/actualizarEstado2', (req, res) => {

    var params = req.body;

    Visitas.findAll({
            attributes: {
                include: [
                    [sequelize.fn('COUNT', sequelize.col('estado')), 'cuentaEstado']
                ],
                exclude: ['id', 'nit', 'orden', 'fechaModificacion', 'observacion', 'idRuta', 'estado']
            },
            where: { idRuta: params.idRuta, estado: 'pendiente' }
        })
        .then((data) => {

            if (data[0].dataValues.cuentaEstado > 0) {
                Rutas.update({ estado: 'terminada', fechaTerminacion: sequelize.fn('NOW') }, { where: { id: params.idRuta } })
                    .then((r) => { res.send(r) })
                    .catch((e) => {
                        console.log(e);
                        res.send(e)
                    })
            } else
                res.send(data)

        })
        .catch((er) => {
            console.log(er);
            res.send(er);
        })

});

router.post('/historialComercial', (req, res) => {


    var paramas = req.body

    var query = "select * from visitas inner join rutas on visitas.idRuta = rutas.id where visitas.estado = 'visitada' and idCliente like '" + paramas.nit + "' and asesor like '" + paramas.asesor + "';";

    Rutas.sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

});


module.exports = router;