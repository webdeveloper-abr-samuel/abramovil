var express = require('express');
const daJ = require('../models/dailyJob');
const sequelize = require('sequelize');
const fs = require('fs');
const {Crypt, Descryp} = require('../helpers/hashData');
//Querys
var $query = require('../queries/dailyJobSql');

var router = express.Router();

var jsonWrite = function(res, ret) {
    if (typeof ret === 'undefined') {
        res.json({
            code: '1',
            msg: 'operation failed'
        });
    } else {
        res.json(ret);
    }
};

//CRYPT
router.post('/GetDailyJob', (req, res) => {

    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var OS = req.headers['user-agent']

    var sql = $query.dailyJobQ.GetDailyJob;

    var params = req.body;

    daJ.sequelize.query(sql, {
            replacements: {
                idCliente: params.idCliente,
                venta: params.venta,
                noVenta: params.noVenta ? params.noVenta : "",
                valorPedido: params.valorPedido ? Crypt(params.valorPedido) : 0,
                obsVenta: params.obsVenta ? Crypt(params.obsVenta) : "",
                nit: Crypt(params.nit),
                Latitude: (params.Latitude ? Crypt(params.Latitude) : Crypt('emptyLat')),
                Longitude: (params.Longitude ? Crypt(params.Longitude) : Crypt('emptyLon')),
                savedBy: (params.savedBy ? params.savedBy : 'user'),
                imgRuta: params.imgRuta ? params.imgRuta : "",
                taskID: params.taskID,
                form: params.form,
                vendedor: params.vendedor ? Crypt(params.vendedor) : "",
                distribuidor: params.distribuidor ? Crypt(params.distribuidor) : "",
                prodAbrVen: params.prodAbrVen ? Crypt(params.prodAbrVen) : ""
            },
            type: daJ.sequelize.QueryTypes.SELECT
        }).then(NewJob => {
            res.json(NewJob);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            })
        })
})

router.post('/GetDailyJob2', (req, res) => {

    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var OS = req.headers['user-agent']

    var sql = $query.dailyJobQ.GetDailyJob2;

    var params = req.body;


    daJ.sequelize.query(sql, {
            replacements: {
                idCliente: params.idCliente,
                venta: params.venta,
                noVenta: params.noVenta ? params.noVenta : "",
                valorPedido: params.valorPedido ? Crypt(params.valorPedido) : 0,
                obsVenta: params.obsVenta ? Crypt(params.obsVenta) : "",
                nit: Crypt(params.nit),
                Latitude: (params.Latitude ? Crypt(params.Latitude) : Crypt('emptyLat')),
                Longitude: (params.Longitude ? Crypt(params.Longitude) : Crypt('emptyLon')),
                savedBy: (params.savedBy ? params.savedBy : 'user'),
                imgRuta: params.imgRuta ? params.imgRuta : "",
                taskID: params.taskID,
                form: params.form,
                vendedor: params.vendedor ? Crypt(params.vendedor) : "",
                distribuidor: params.distribuidor ? Crypt(params.distribuidor) : "",
                prodAbrVen: Crypt(params.prodAbrVen),
                asesorDistribuidor: Crypt(params.asesorDis)
            },
            type: daJ.sequelize.QueryTypes.SELECT
        }).then(NewJob => {
            var c = 0;
            for (i = 0; i < params.detalleOrden.length; i++) {
                
                daJ.sequelize.query("INSERT INTO abrageo.detalleordens(idGestion,code,referencia,valor,cantidad)VALUES(" + NewJob[0][0].rpta + ",'" + params.detalleOrden[i].categoria + "','" + params.detalleOrden[i].producto + "'," + params.detalleOrden[i].precio + "," + params.detalleOrden[i].cantidad + ");", { type: daJ.sequelize.QueryTypes.INSERT })
                    .then(data => {
                        console.log(c)
                        if (c == (params.detalleOrden.length - 1)) {
                            res.json({ idGestion: NewJob[0][0].rpta, mensaje: 'se guardo correctamente la orden' });
                        } else {
                            c++
                        }
                    })
            }

        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            })
        })
})


//NO CRYPT
router.post('/UploadInvoice', (req, res) => {
    try {
        var datetime = new Date();

        var folderName = './API/files/Daily/' + datetime.toISOString().slice(0, 10)

        if (!fs.existsSync(folderName))
            fs.mkdirSync(folderName)

        folderName = './API/files/Daily/' + datetime.toISOString().slice(0, 10) + '/' + req.files.file.name.split('.')[0]

        if (!fs.existsSync(folderName))
            fs.mkdirSync(folderName)

        let EDFile = req.files.file
        EDFile.mv(folderName + '/' + datetime.getMinutes() + '_' + datetime.getSeconds() + '_' + req.files.file.name,
            err => {
                if (err)
                    return res.status(500).send({ message: err })
                else {
                    res.json(folderName + '/' + datetime.getMinutes() + '_' + datetime.getSeconds() + '_' + req.files.file.name);
                }
            })
    } catch (err) {
        console.error(err)
    }
})


//DECRYP
router.post('/ejecucion', (req, res) => {
//encritar guardadopor y desencriptar la data

    // 190.130.72.6
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var OS = req.headers['user-agent']
    let data = [];

    var sql = $query.dailyJobQ.ejecucion;
    const {fechaInicial, fechaFinal, guardadoPor} = req.body;
    daJ.sequelize.query(sql, {
            replacements: {
                fechaInicial,
                fechaFinal,
                guardadoPor: Crypt(guardadoPor)
            },
            type: daJ.sequelize.QueryTypes.SELECT
        }).then(result => {
            result.forEach(element => {
               data.push({
                id: element.id,
                venta: element.venta,
                noVenta: element.noVenta,
                valorPedido: Descryp(element.valorPedido),
                obsVenta: Descryp(element.obsVenta),
                ingresoFH: element.ingresoFH,
                vendedor: Descryp(element.vendedor),
                distribuidor: Descryp(element.distribuidor),
                prodAbrVen: Descryp(element.prodAbrVen),
                nombreNegocio: Descryp(element.nombreNegocio),
                estrato: Descryp(element.estrato),
                nit: Descryp(element.nit),
                telefono: Descryp(element.telefono),
                contacto: Descryp(element.contacto),
                cargo: Descryp(element.cargo),
                correo: Descryp(element.correo),
                pais: element.pais,
                departamento: element.departamento,
                ciudadPoblacion: element.ciudadPoblacion,
                territorio: element.territorio,
                nroComunaZona: element.nroComunaZona,
                nombreComZon: element.nombreComZon,
                barrio: element.barrio,
                clasificacion: Descryp(element.clasificacion),
                tipologia: Descryp(element.tipologia),
                comentarios: element.comentarios,
                direccion: Descryp(element.direccion),
                pubExte: element.pubExte,
                pubInte: element.pubInte,
                exhibi: element.exhibi,
                lat: Descryp(element.lat),
                lng: Descryp(element.lng),
                prodAbrasivos: Descryp(element.prodAbrasivos),
                lineasS: Descryp(element.lineasS),
                promCompr: Descryp(element.promCompr),
                UnCorteF: Descryp(element.UnCorteF),
                marcas: Descryp(element.marcas),
                unFlapD: Descryp(element.unFlapD),
                marcasFD: Descryp(element.marcasFD),
                promLS: Descryp(element.promLS),
                maLijS: Descryp(element.maLijS),
               })
            });
            res.json(data);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            })
        })
})

module.exports = router;