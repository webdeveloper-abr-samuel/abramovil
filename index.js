require('dotenv').config();
const express = require('express');
const app = express();
const fileUpload = require('express-fileupload');
// const tls = require('tls');
// const https = require("https")
// const helmet = require("helmet");
var cors = require('cors');


var corser = require('corser');

// Setting
app.set('port', process.env.PORT || 3003)
const CUSTOMER = require('./API/customer');
const TASK = require('./API/tasks');
const DAILYJOB = require('./API/dailyjob');
const ABRAGEO = require('./API/abrageo');
const ROUTER = require('./API/router');
const MAPA = require('./API/mapa');
const RECAUDO = require('./API/recaudo');
const CUENTA = require('./API/cuentaClave');
const INDICADORES = require('./API/indicadores');;

//middlewares
app.use(cors())
app.use(corser.create({
    methods: corser.simpleMethods.concat(["PUT"]),
    requestHeaders: corser.simpleRequestHeaders.concat(["X-Requested-With"])
}));
app.use(fileUpload());
app.all('*', function(request, response, next) {
    response.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,Authorization,Access-Control-Allow-Origin');
    response.header('Access-Control-Allow-Methods', 'POST,GET,DELETE');
    response.header('Access-Control-Allow-Origin', '*');
    next();
});
// app.use(helmet());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));


//administrador
app.use('/js', express.static(__dirname + '/public/js'))
app.use('/css', express.static(__dirname + '/public/css'))
app.use('/fonts', express.static(__dirname + '/public/fonts'))
app.use('/img', express.static(__dirname + '/public/img'))

app.use('/API/files', express.static(__dirname + '/API/files'))


app.use('/',express.static(__dirname + '/public'))

//Routes
app.use('/CUSTOMER', CUSTOMER);
app.use('/TASK', TASK);
app.use('/DAILYJOB', DAILYJOB);
app.use('/ABRAGEO', ABRAGEO);
app.use('/ROUTER', ROUTER);
app.use('/MAPA', MAPA);
app.use('/RECAUDO', RECAUDO);
app.use('/CUENTA', CUENTA);
app.use('/INDICADORES',INDICADORES);


//Server online
app.listen(app.get('port'), () => {
    console.log("Server on port ", app.get('port'))
})