const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'visitas',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    idRuta: {
      type: Sequelize.INTEGER,

    },
    idCliente: {
        type: Sequelize.INTEGER,
    },
    orden: {
        type: Sequelize.INTEGER,
    },
    estado: {
      type: Sequelize.STRING,
      length: 50
    },
    fechaModificacion: {
      type: Sequelize.DATE
    },
    observacion: {
      type: Sequelize.STRING,
      length: 100
    },

  },
  {
    timestamps: false,
    freezeTableName: true,
  }
)
