var express = require('express');
const daJ = require('../models/dailyJob');
const Cust = require('../models/Cust');
const Visitas = require('../models/Visitas')
const Rutas = require('../models/Rutas')
const sequelize = require('sequelize');
const fs = require('fs');
const Op = sequelize.Op;
const jwt = require('jsonwebtoken');
const config = require('./config/config');
//Querys
var $query = require('../queries/abraGeoSql.js');
const dbEpicor = require('../database/dbEpicor.js')
const dbCuentas = require('../database/dbCuentas.js')
const {Crypt, Descryp} = require('../helpers/hashData');


var router = express.Router();

var jsonWrite = function(res, ret) {
    if (typeof ret === 'undefined') {
        res.json({
            code: '1',
            msg: 'operation failed'
        });
    } else {
        res.json(ret);
    }
};

var pushIndicesToArray = function(inputString, strToSearch) {
    let indices = []
    let startIndex = 0
    let index

    while ((index = inputString.indexOf(strToSearch, startIndex)) > -1) {
        indices.push(index)
        startIndex = index + strToSearch.length
    }

    return indices
}

var dataToArray = function(inputString, indexArray) {
    var startIndex = -1,
        array = []

    for (let index = 0; index <= indexArray.length; index++) {
        array.push(inputString.substring(startIndex + 1, indexArray[index]))
        startIndex = indexArray[index]
    }
    return array
}

function jwtRegUser(user) {
    const ONE_HOUR = 60 * 60;
    return jwt.sign(JSON.parse(JSON.stringify(user)), config.authentication.jwtSecret, {
        expiresIn: ONE_HOUR
    });
}

// ABRAGEO
router.post('/amplitudPortafolio', (req, res) => {
    // var sql = $query.abraGeoQ.GetDailyJob;

    var params = req.body;

    let data = [0, 0, 0];
    let len = [];
    const manager = Crypt('manager');
    const TI = Crypt('TI');
    const Thernandez = Crypt('Thernandez');

    daJ.findAll({
            attributes: ['prodAbrVen'],
            where: {
                ingresoFH: {
                    [Op.gte]: new Date(params.fechaInicio),
                    [Op.lte]: new Date(params.fechaFin)
                },
                prodAbrVen: {
                    [Op.ne]: ['']
                },
                savedBy: {
                    [Op.ne]: manager,
                    [Op.ne]: TI ,
                    [Op.ne]: Thernandez
                }
            }
        }).then(pAV => {

            for (let index = 0; index < pAV.length; index++) {
                len.push(dataToArray(pAV[index].prodAbrVen, pushIndicesToArray(pAV[index].prodAbrVen, ",")).length);

                if (len[index] > 3)
                    data[2]++;
                else if (len[index] > 1 && len[index] <= 3)
                    data[1]++;
                else
                    data[0]++;
            }

            res.json({
                dataSet: len,
                mayorTresProd: data[2],
                entreTresUnProd: data[1],
                unProd: data[1],
            });
        })
        .catch(err => {
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            });
        });
});

router.post('/publicidad', (req, res) => {

    Cust.sequelize.query('Call publicidad()')
        .then(datos => {
            res.json({
                si: [datos[0].pExSi, datos[0].pInSi, datos[0].pExhSi],
                no: [datos[0].pInNo, datos[0].pInNo, datos[0].pExhNo]
            });
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

//Decrypt - Pendiente
router.post('/ventaEfecti', (req, res) => {
//encriptar params asesor y desenciprtat los datos
    const { asesor,regional } = req.body;
    //let hashAsesor = Crypt(asesor)

    Cust.sequelize.query("call ventasEfectividad('" + asesor + "','" + regional + "')")
        .then(datos => {
            res.json(datos);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

//Decrypt - Pendiente
router.post('/visitaEfecti', (req, res) => {

    const { asesor,regional } = req.body;


    Cust.sequelize.query("call visitasEfectividad('" + asesor + "','" + regional + "')")
        .then(datos => {
            res.json(datos);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

//Decrypt-hecho
router.get('/listaRegAdd', (req, res) => {
    let dat = new Date()

    let month
    let month1
    let data = [];

    if ((dat.getMonth() + 1) < 10) {
        month = '0' + dat.getMonth() + 1
    }

    if (dat.getMonth() < 10) {
        month1 = '0' + dat.getMonth()
    }

    Cust.sequelize.query("select savedBy as userName from gestiondiaria where savedBy <> '' and (ingresoFH like '%" + dat.getFullYear() + "-" + month + "%' or ingresoFH like '%" + dat.getFullYear() + "-" + month1 + "%') group by savedBy;", { type: Cust.sequelize.QueryTypes.SELECT })
        .then(datos => {
            datos.forEach(element => {
                data.push({
                    userName: Descryp(element.userName)
                })
            });
            res.json(data);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

router.post('/listaAsesorPorReg', (req, res) => {

    var params = req.body;

    Cust.sequelize.query("select userName from asesoresregional where regional = '" + params.regional + "' and '" + params.regional + "' <> ''  or  '" + params.regional + "' = '' ;")
        .then(datos => {
            res.json(datos);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

//Decrypt-hecho
router.get('/getAll', (req, res) => {
//encriptar creadoPor y desencriptar el resultado
// desencriptar valores en el for

    const manager = Crypt('manager');
    const TI = Crypt('TI');
    const Thernandez = Crypt('Thernandez');

    Cust.findAll({
            attributes: [
                'nombreNegocio', 'estrato', 'nit', 'telefono', 'contacto',
                'cargo', 'correo', 'pais', 'departamento', 'ciudadPoblacion',
                'territorio', 'nombreComZon', 'barrio', 'clasificacion', 'tipologia',
                'comentarios', 'direccion', 'creadoPor', 'creacionFH', 'modificadoPor',
                'modificacionFH', 'pubExte', 'pubInte', 'exhibi', 'lat',
                'lng', 'origenDatos', 'foto', 'UnCorteF', 'unFlapD', 'promLS'
            ],
            where: {
                creadoPor: {
                    [Op.ne]: manager,
                    [Op.ne]: TI,
                    [Op.ne]: [''],
                    [Op.ne]: Thernandez
                },
                //foto: {[Op.ne]: ''}
            },
            order: [
                    ['creacionFH', 'DESC']
                ]
                //limit: 3
        }).then(data => {
            let global = []

            for (let posi in data) {

                //DECRYPT
                data[posi].dataValues.clasificacion = Descryp(data[posi].dataValues.clasificacion)
                data[posi].dataValues.nombreNegocio = Descryp(data[posi].dataValues.nombreNegocio)
                data[posi].dataValues.tipologia = Descryp(data[posi].dataValues.tipologia)
                data[posi].dataValues.lng = Descryp(data[posi].dataValues.lng)
                data[posi].dataValues.lat = Descryp(data[posi].dataValues.lat)

                let color = '';
                if (data[posi].dataValues.clasificacion == 'AA')
                    color = "#638DC8"
                else if (data[posi].dataValues.clasificacion == 'A')
                    color = "#DA802D"
                else if (data[posi].dataValues.clasificacion == 'B')
                    color = "#B0CC49"
                else if (data[posi].dataValues.clasificacion == 'C')
                    color = "#CB2128"
                else
                    color = "#FFF02D"




                let regional = ''
                if (data[posi].dataValues.nombreNegocio == 'Lleal' || data[posi].dataValues.nombreNegocio == 'KORDONEZ')
                    regional = "occidente"
                else(data[posi].dataValues.clasificacion == 'RMORENO')
                regional = "norte"



                let IconIMG = ''


                if (data[posi].dataValues.tipologia == 'Ferreterías en general') {
                    if (data[posi].dataValues.clasificacion == 'AA')
                        IconIMG = "FerreteriaAA.png"
                    else if (data[posi].dataValues.clasificacion == 'A')
                        IconIMG = "FerreteriaA.png"
                    else if (data[posi].dataValues.clasificacion == 'B')
                        IconIMG = "FerreteriaB.png"
                    else if (data[posi].dataValues.clasificacion == 'C')
                        IconIMG = "FerreteriaC.png"
                    else
                        IconIMG = "Ferreterias.png"
                } else if (data[posi].dataValues.tipologia == 'Depósitos de materiales') {
                    if (data[posi].dataValues.clasificacion == 'AA')
                        IconIMG = "DepositosAA.png"
                    else if (data[posi].dataValues.clasificacion == 'A')
                        IconIMG = "DepositosA.png"
                    else if (data[posi].dataValues.clasificacion == 'B')
                        IconIMG = "DepositosB.png"
                    else if (data[posi].dataValues.clasificacion == 'C')
                        IconIMG = "DepositosC.png"
                    else
                        IconIMG = "Depositos.png"
                } else if (data[posi].dataValues.tipologia == 'Almacenes de Pinturas Arquitectonicas') {
                    if (data[posi].dataValues.clasificacion == 'AA')
                        IconIMG = "PinturasAA.png"
                    else if (data[posi].dataValues.clasificacion == 'A')
                        IconIMG = "PinturasA.png"
                    else if (data[posi].dataValues.clasificacion == 'B')
                        IconIMG = "PinturasB.png"
                    else if (data[posi].dataValues.clasificacion == 'C')
                        IconIMG = "PinturasC.png"
                    else
                        IconIMG = "Pinturas.png"
                } else if (data[posi].dataValues.tipologia == 'Otros' || data[posi].dataValues.tipologia == 'Ferretería Industrial' || data[posi].dataValues.tipologia == 'Almacenes del Agro') {
                    if (data[posi].dataValues.clasificacion == 'AA')
                        IconIMG = "OtrosAA.png"
                    else if (data[posi].dataValues.clasificacion == 'A')
                        IconIMG = "OtrosA.png"
                    else if (data[posi].dataValues.clasificacion == 'B')
                        IconIMG = "OtrosB.png"
                    else if (data[posi].dataValues.clasificacion == 'C')
                        IconIMG = "OtrosC.png"
                    else
                        IconIMG = "Otros.png"
                } else if (data[posi].dataValues.tipologia == 'Almacen de Eléctricos') {
                    if (data[posi].dataValues.clasificacion == 'AA')
                        IconIMG = "ElectricosAA.png"
                    else if (data[posi].dataValues.clasificacion == 'A')
                        IconIMG = "ElectricosA.png"
                    else if (data[posi].dataValues.clasificacion == 'B')
                        IconIMG = "ElectricosB.png"
                    else if (data[posi].dataValues.clasificacion == 'C')
                        IconIMG = "ElectricosC.png"
                    else
                        IconIMG = "Pinturas.png"
                } else
                    IconIMG = "Otros.png"

                global.push({
                    type: 'Feature',
                    properties: {
                        nombreNegocio: data[posi].dataValues.nombreNegocio,
                        estrato: Descryp(data[posi].dataValues.estrato),
                        pais: data[posi].dataValues.pais,
                        departamento: data[posi].dataValues.departamento,
                        ciudadPoblacion: data[posi].dataValues.ciudadPoblacion,
                        barrio: data[posi].dataValues.barrio,
                        clasificacion: data[posi].dataValues.clasificacion,
                        tipologia: data[posi].dataValues.tipologia,
                        pubExte: data[posi].dataValues.pubExte,
                        pubInte: data[posi].dataValues.pubInte,
                        exhibi: data[posi].dataValues.exhibi,
                        "marker-color": color,
                        "marker-symbol": "star", // https://map.michelstuyts.be/icons/
                        "maker-size": "large",
                        regional: regional,
                        foto: data[posi].dataValues.foto,
                        contacto: data[posi].dataValues.contacto,
                        telefono: data[posi].dataValues.telefono,
                        direccion: Descryp(data[posi].dataValues.direccion),
                        creadoPor: data[posi].dataValues.creadoPor,
                        modificadoPor: data[posi].dataValues.modificadoPor,
                        creacion: data[posi].dataValues.creacionFH,
                        modificacion: data[posi].dataValues.modificacionFH,
                        territorio: data[posi].dataValues.territorio,
                        corteFino: data[posi].dataValues.UnCorteF,
                        flap: data[posi].dataValues.unFlapD,
                        lija: data[posi].dataValues.promLS,
                        icon: {
                            iconUrl: './img/' + IconIMG,
                            iconSize: [55, 70],
                            iconAnchor: [50, 50]
                        }
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [parseFloat(data[posi].dataValues.lng), parseFloat(data[posi].dataValues.lat)]
                    }
                });
            }
            res.json({ "type": "FeatureCollection", "features": global });
        })
        .catch(err => {
            console.log(err);
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            });
        });

});

//Decrypt-hecho
router.post('/getAll2', (req, res) => {

    var params = req.body

    Cust.findAll({
            attributes: [
                'nombreNegocio', 'estrato', 'nit', 'telefono', 'contacto',
                'cargo', 'correo', 'pais', 'departamento', 'ciudadPoblacion',
                'territorio', 'nombreComZon', 'barrio', 'clasificacion', 'tipologia',
                'comentarios', 'direccion', 'creadoPor', 'creacionFH', 'modificadoPor',
                'modificacionFH', 'pubExte', 'pubInte', 'exhibi', 'lat',
                'lng', 'origenDatos', 'foto'
            ],
            where: {
                nit: {
                    [Op.ne]: ['0']
                },
                departamento: {
                    [Op.eq]: [params.departamento]
                },
            },
            limit: 3
        }).then(data => {
            let global = []

            for (let posi in data) {

                data[posi].dataValues.lng = Descryp(data[posi].dataValues.lng)
                data[posi].dataValues.lat= Descryp(data[posi].dataValues.lat)

                global.push({
                    type: 'Feature',
                    properties: {
                        nombreNegocio: Descryp(data[posi].dataValues.nombreNegocio),
                        estrato: Descryp(data[posi].dataValues.estrato),
                        nit: Descryp(data[posi].dataValues.nit),
                        pais: data[posi].dataValues.pais,
                        departamento: data[posi].dataValues.departamento,
                        ciudadPoblacion: data[posi].dataValues.ciudadPoblacion,
                        barrio: data[posi].dataValues.barrio,
                        clasificacion: Descryp(data[posi].dataValues.clasificacion),
                        tipologia: Descryp(data[posi].dataValues.tipologia),
                        pubExte: data[posi].dataValues.pubExte,
                        pubInte: data[posi].dataValues.pubInte,
                        exhibi: data[posi].dataValues.exhibi,
                        foto: data[posi].dataValues.foto
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [parseFloat(data[posi].dataValues.lng), parseFloat(data[posi].dataValues.lat)]
                    }
                });
            }
            res.json({ "type": "FeatureCollection", "features": global });
        })
        .catch(err => {
            console.log(err);
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            });
        });

});

//Decryp-pendiente
router.post('/getAll3', (req, res) => {
// encriptar params y decrypt data
    var params = req.body

    if (params.prodAbrasivos !== '')
        params.prodAbrasivos = "%" + params.prodAbrasivos + "%"


    Cust.sequelize.query("call abrageo.filtrosRutero('" + params.departamento + "','" + params.ciudad + "','" + params.comuna + "','" + params.barrio + "','" + params.clasificacion + "','" + params.tipologia + "','" + params.prodAbrasivos + "','" + params.distribuidor + "')")
        .then(datos => {
            res.json(datos);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});


router.post('/asignarRuta', (req, res) => {

    var idClientes = req.body.idClientes.split(",")

    Rutas.create(req.body)
        .then(Update => {

            let auxArray = []

            for (let index = 0, j = idClientes.length; index < j; index++) {
                if (idClientes[index] !== "") {
                    auxArray.push({ idRuta: Update.id, idCliente: idClientes[index], orden: index + 1 }, )
                }

            }

            Visitas.bulkCreate(auxArray).then(data => {
                res.json({ data: data, routeCreated: 'ok', idRuta: Update.id })
            }).catch(err => {
                console.log(err)
                res.send(err)
            })

        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            })
        })

});

//Decrypt-hecho
router.get('/fichaCliente', (req, res) => {

    let manager = Crypt('manager')
    let TI = Crypt('TI')
    let Thernandez = Crypt('Thernandez')
    let data = []

    Cust.findAll({
            attributes: [
                'nombreNegocio', 'estrato', 'nit', 'telefono', 'contacto',
                'cargo', 'correo', 'pais', 'departamento', 'ciudadPoblacion',
                'territorio', 'nombreComZon', 'barrio', 'clasificacion', 'tipologia',
                'comentarios', 'direccion', 'creadoPor', 'creacionFH', 'modificadoPor',
                'modificacionFH', 'pubExte', 'pubInte', 'exhibi', 'lat',
                'lng', 'origenDatos'
            ],
            where: {
                creadoPor: {
                    [Op.ne]: manager,
                    [Op.ne]: TI,
                    [Op.ne]: [''],
                    [Op.ne]: Thernandez
                }
            }
        }).then(datos => {
            datos.forEach(element => {
                data.push({
                    nombreNegocio: Descryp(element),
                    estrato: Descryp(element.estrato),
                    nit: Descryp(element.nit),
                    telefono: Descryp(element.telefono),
                    contacto: Descryp(element.contacto),
                    cargo: Descryp(element.cargo),
                    correo: Descryp(element.correo),
                    pais: element.pais,
                    departamento: element.departamento,
                    ciudadPoblacion: element.ciudadPoblacion,
                    territorio: element.territorio,
                    nombreComZon: element.nombreComZon,
                    barrio: element.barrio,
                    clasificacion: Descryp(element.clasificacion),
                    tipologia: Descryp(element.tipologia),
                    comentarios: element.comentarios,
                    direccion: Descryp(element.direccion),
                    creadoPor: element.creadoPor,
                    creacionFH: element.creacionFH,
                    modificadoPor: element.modificadoPor,
                    modificacionFH: element.modificacionFH,
                    pubExte: element.pubExte,
                    pubInte: element.pubInte,
                    exhibi: element.exhibi,
                    lat: Descryp(element.lat),
                    lng: Descryp(element.lng),
                    origenDatos: element.origenDatos
                })
            });
            res.send(data)
        })
        .catch(err => {
            console.log(err);
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            });
        });

});

//Decrypt-pendiente
router.get('/gestionDiaria', (req, res) => {

    daJ.findAll({
            attributes: [
                'prodAbrasivos', 'lineasS', 'promCompr', 'UnCorteF', 'marcas',
                'unFlapD', 'marcasFD', 'promLS', 'maLijS', 'venta',
                'noVenta', 'valorPedido', 'obsVenta', 'nit', 'Latitude',
                'Longitude', 'savedBy', 'ingresoFH', 'vendedor', 'distribuidor',
                'prodAbrVen'
            ],
            where: {
                savedBy: {
                    [Op.ne]: ['manager'],
                    [Op.ne]: ['TI'],
                    [Op.ne]: ['Thernandez']
                }
            }
        }).then(data => {
            res.send(data)
        })
        .catch(err => {
            console.log(err);
            res.json({
                'state': 'error',
                'data': JSON.stringify(err),
            });
        });

});

//Decrypt-hecho
router.get('/fichaClienteGD', (req, res) => {
    let data = [];
    Cust.sequelize.query("select DISTINCT * from fichacliente", { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            datos.forEach(element => {
                data.push({
                    id: element.id,
                    nombreNegocio: Descryp(element.nombreNegocio),
                    estrato: Descryp(element.estrato),
                    nit: Descryp(element.nit),
                    telefono: Descryp(element.telefono),
                    contacto: Descryp(element.contacto),
                    cargo: Descryp(element.cargo),
                    correo: Descryp(element.correo),
                    pais: element.pais,
                    departamento: element.departamento,
                    ciudadPoblacion: element.ciudadPoblacion,
                    territorio: element.territorio,
                    nroComunaZona: element.nroComunaZona,
                    nombreComZon: element.nombreComZon,
                    barrio: element.barrio,
                    clasificacion: Descryp(element.clasificacion),
                    tipologia: Descryp(element.tipologia),
                    ordenRuta: element.ordenRuta,
                    comentarios: element.comentarios,
                    direccion: Descryp(element.direccion),
                    creadoPor: element.creadoPor,
                    creacionFH: element.creacionFH,
                    modificadoPor: element.modificadoPor,
                    modificacionFH: element.modificacionFH,
                    pubExte: element.pubExte,
                    pubInte: element.pubInte,
                    exhibi: element.exhibi,
                    lat: Descryp(element.lat),
                    lng : Descryp(element.lng),
                    gooSitioId: element.gooSitioId,
                    foto: element.foto,
                    origenDatos:element.origenDatos,
                    prodAbrasivos: Descryp(element.prodAbrasivos),
                    lineasS: Descryp(element.lineasS),
                    promCompr: Descryp(element.promCompr),
                    UnCorteF: Descryp(element.UnCorteF),
                    marcas: Descryp(element.marcas),
                    unFlapD: Descryp(element.unFlapD),
                    marcasFD: Descryp(element.marcasFD),
                    promLS: Descryp(element.promLS),
                    maLijS: Descryp(element.maLijS),
                    numVerParPer: element.numVerParPer,
                    canal: element.canal
                })
            });
            res.json(data);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

//Decrypt-hecho
router.get('/gestionDiariaV', (req, res) => {
    let data = [];
    var sql = $query.gD;

    Cust.sequelize.query(sql, { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            datos.forEach(element => {
                data.push({
                    venta: element.venta,
                    noVenta: element.noVenta,
                    valorPedido: Descryp(element.valorPedido),
                    obsVenta: Descryp(element.obsVenta),
                    nit: Descryp(element.nit),
                    Latitude: Descryp(element.Latitude),
                    Longitude: Descryp(element.Longitude),
                    savedBy: Descryp(element.savedBy),
                    ingresoFH: element.ingresoFH,
                    imgRuta: element.imgRuta,
                    taskID: element.taskID,
                    form: element.form,
                    vendedor: Descryp(element.vendedor),
                    distribuidor: Descryp(element.distribuidor),
                    prodAbrVen: Descryp(element.prodAbrVen),
                    nombreNegocio: Descryp(element.nombreNegocio),
                    direccion: Descryp(element.direccion),
                    barrio: element.barrio,
                    tipologia: Descryp(element.tipologia),
                    nombreComZon: element.nombreComZon,
                    ciudadPoblacion: element.ciudadPoblacion,
                    telefono: Descryp(element.telefono),
                    departamento: element.departamento,
                })
            });
            res.json(data);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

router.post('/loginAppUser', (req, res) => {

    var params = req.body;

    Cust.sequelize.query("select email, profile from appusers where email = '" + params.email + "' and password = MD5(sha1('" + params.pwd + "'));", { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            res.send({
                user: datos[0].email,
                profile: datos[0].profile,
                token: jwtRegUser(datos[0])
            })
        })
        .catch(err => {
            console.log(err)
            res.status(403).send({
                error: 'Invalid login credentials'
            })
        });

})

router.post('/seguimiento', (req, res) => {

    var params = req.body;

    Cust.sequelize.query("insert into seguimiento(imei,fecha,latitud,longitud,observaciones) values('" +
            params.imei + "',now(),'" +
            params.latitud + "','" +
            params.longitud + "','" +
            params.observaciones + "')", { type: sequelize.QueryTypes.INSERT })
        .then(datos => {
            res.json(datos);
        })
        .catch(err => {
            console.log(err)
            res.json({
                'state': 'error',
                'data': err
            });
        });

});

//Decrypt-hecho
router.get('/animationData', (req, res) => {
    Cust.sequelize.query('call animationData', { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            let position = [];
            for (let i in datos[0]) {
                position.push({
                    lon: Descryp(datos[0][i].lng),
                    lat: Descryp(datos[0][i].lat),
                    ciudad: datos[0][i].ciudadPoblacion
                })
            }
            console.log(position)
            res.status(200).send({ positions: position });
        })
})

router.post('/datoHora', (req, res) => {
    console.log(req);
    Cust.sequelize.query("call dataHour(:hour)", { replacements: { hour: req.body['hora'] } })
        .then(datos => {

            res.status(200).send({ Datos: datos });
        })
        .catch(err => {
            console.log(err)
            res.status(400).send({ err: err })
        })
})

router.post('/dataComerciales', (req, res) => {
    dbEpicor.sequelize.query("select SalesRepCode as Code, Name from SalesRep where SalesRepTitle = 'VENDEDOR';", { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            res.status(200).send({ Datos: datos });
        })
        .catch(err => {
            console.log(err)
            res.status(400).send({ err: err })
        })
})

router.post('/clienteComercial', (req, res) => {
    console.log(req.body)
    dbEpicor.sequelize.query("Select Distinct Top 40 C.CustNum,C.ResaleID,C.Name,C.Address1,C.City,C.State,CreditHold,C.PhoneNum,CreditLimit From Customer C Inner Join ShipTo SH With(NoLock) On (C.CustNum = SH.CustNum And C.Company = SH.Company) Where C.Company = 'ABRACOL' And C.Name Not Like '%VENTAS%' And C.ResaleID Not In ('300000') And  (( '' <> '' And  (C.ResaleID Like '%%' Or C.Name Like '%%') ) Or ('' = '')) And ( (SH.SalesRepCode = '" + req.body.code + "' And '" + req.body.code + "' NOT IN ('','ALL')) Or ('" + req.body.code + "' IN ('','ALL'))  ) And SH.ShipToNum <> '';", { type: sequelize.QueryTypes.SELECT })
        .then(datos => {
            res.status(200).send({ Datos: datos });
        })
        .catch(err => {
            console.log(err)
            res.status(400).send({ err: err })
        })
})

router.post('/dataUserComercial', (req, res) => {
    var params = req.body;

    var query = "select * from Erp.UserComp where DcdUserID like '" + params.asesor + "%';";
    //var query = "select * from cliente where agregado like '" + params.asesor + "%';";
    dbEpicor.sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.get('/hojaVida', (req, res) => {
    var params = req.body;

    var query = `select cliente.nit, nombre as cliente, tipo, tamaño, frecuencia, ventas_actuales, ventas_proyectadas, lista_precio, pac, informacion_comercial,
    productos_compra, productos_codificar as 'productos_codificados', antiguedad_empresa, antiguedad_cliente, areas_empresa, contactos, forma_pago
    observaciones, fortaleza, debilidad, oportunidad, amenazas, agregado as usuario
    from cliente inner join dofa on cliente.nit = dofa.nit where agregado not like 'thernandez';`;
    //var query = "select * from cliente where agregado like '" + params.asesor + "%';";
    dbCuentas.sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.get('/gestionComercial', (req, res) => {
    var params = req.body;

    var query = `select visitas.id, cliente.nombre, idCliente, date_format(fechaModificacion, '%d-%m-%Y') as Fecha, 
    visitas.observacion, areaAtendida, tema, informacion, compromiso, agregado as usuario, observacionRegional, ObservacionGerente, fechaRegional, fechaGerente  from cliente 
    inner join abrageo.visitas on cliente.nit = visitas.idCliente where agregado not like 'thernandez' and 
    areaAtendida is not null;`;
    //var query = "select * from cliente where agregado like '" + params.asesor + "%';";
    dbCuentas.sequelize.query(
            query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.post('/guardarObservacion', (req, res) => {
    var params = req.body;

    if (params.observacionRegional != '' && params.observacionGerente == '') {
        var query = `update visitas set observacionRegional = "` + params.observacionRegional + `", fechaRegional=now(), observacionGerente = "" where id = "` + params.id + `";`;
    }

    if (params.observacionRegional != '' && params.observacionGerente != '') {
        var query = `update visitas set observacionRegional = "` + params.observacionRegional + `", observacionGerente = "` + params.observacionGerente + `", fechaGerente=now() where id = "` + params.id + `";`;
    }
    //var query = "select * from cliente where agregado like '" + params.asesor + "%';";
    dbCuentas.sequelize.query(
            query, { type: sequelize.QueryTypes.INSERT })
        .then(task => {
            res.send({ mensaje: 'se guardo correctamente' })
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

router.post('/getAgent', (req, res) => {
    var params = req.body;


    var query = `SELECT * FROM abrageo.appusers where distribuidor like "` + params.distribuidor + `";`;
    //var query = "select * from cliente where agregado like '" + params.asesor + "%';";
    dbCuentas.sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
        .then(task => {
            res.send(task)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
})

/* *************************GRAFICAS********************** */

/* --------------------------------CONSULT SQL-------------------------------- */
function ValorDistribuidor(fecha,departamento,ciudad,barrio,distribuidor) {
    return `SELECT valorPedido, Date_format(gestiondiaria.ingresoFH,'%M','es_ES') as name
            FROM gestiondiaria
            inner join fichacliente f ON f.id = gestiondiaria.idCliente
            WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
            AND f.departamento LIKE "%${departamento}%"
            AND f.ciudadPoblacion LIKE "%${ciudad}%"
            AND f.barrio LIKE "%${barrio}%"
            AND gestiondiaria.distribuidor LIKE "%${distribuidor}%"
            GROUP BY ingresoFH ASC`
}

function ValorDia(fecha,departamento,ciudad,barrio,distribuidor) {
    return `SELECT valorPedido, DATE_FORMAT(gestiondiaria.ingresoFH, "%d") as dia
            FROM gestiondiaria
            inner join fichacliente f ON f.id = gestiondiaria.idCliente
            WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
            AND f.departamento LIKE "%${departamento}%"
            AND f.ciudadPoblacion LIKE "%${ciudad}%"
            AND f.barrio LIKE "%${barrio}%"
            AND gestiondiaria.distribuidor LIKE "%${distribuidor}%"
            ORDER BY DATE_FORMAT(gestiondiaria.ingresoFH, "%d") ASC`
}

function ValorDiaASesor(fecha,departamento,ciudad,barrio,asesor) {
    return `SELECT valorPedido, DATE_FORMAT(gestiondiaria.ingresoFH, "%d") as dia
            FROM gestiondiaria
            inner join fichacliente f ON f.id = gestiondiaria.idCliente
            WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
            AND f.departamento LIKE "%${departamento}%"
            AND f.ciudadPoblacion LIKE "%${ciudad}%"
            AND f.barrio LIKE "%${barrio}%"
            AND gestiondiaria.savedBy LIKE "%${asesor}%"
            ORDER BY DATE_FORMAT(gestiondiaria.ingresoFH, "%d") ASC`
}

function States() {
    return `SELECT DISTINCT(fichacliente.departamento) 
            FROM fichacliente 
            WHERE fichacliente.departamento != ""`
}

function FilterCiudad(state) {
    return `SELECT  DISTINCT (fichacliente.ciudadPoblacion)
            FROM fichacliente 
            WHERE fichacliente.departamento = '${state}'
            AND fichacliente.ciudadPoblacion != ""`
}

function FilterBarrio(state,city) {
    return `SELECT  DISTINCT (fichacliente.barrio)
            FROM fichacliente 
            WHERE fichacliente.departamento = '${state}'
            AND fichacliente.ciudadPoblacion = '${city}'
            AND fichacliente.barrio != ""`
}
  
function FilterDistributor() {
return `SELECT  DISTINCT (gestiondiaria.distribuidor)
        FROM gestiondiaria
        WHERE gestiondiaria.distribuidor != ""
        ORDER BY gestiondiaria.distribuidor ASC`
}
  
function FilterAsesor() {
return `SELECT  DISTINCT (gestiondiaria.savedBy)
        FROM gestiondiaria
        WHERE gestiondiaria.savedBy != ""
        ORDER BY gestiondiaria.savedBy ASC`
}
  
function ValorAsesor(fecha,departamento,ciudad,barrio,asesor) {
return `SELECT valorPedido, Date_format(gestiondiaria.ingresoFH,'%M','es_ES') as name
        FROM gestiondiaria
        inner join fichacliente f ON f.id = gestiondiaria.idCliente
        WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
        AND f.departamento LIKE "%${departamento}%"
        AND f.ciudadPoblacion LIKE "%${ciudad}%"
        AND f.barrio LIKE "%${barrio}%"
        AND gestiondiaria.savedBy LIKE "%${asesor}%"`
}

function SumYearsAsesor(fecha,departamento,ciudad,barrio,asesor) {
    return `SELECT SUM(valorPedido) as total
    FROM gestiondiaria
    inner join fichacliente f ON f.id = gestiondiaria.idCliente
    WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
    AND f.departamento LIKE "%${departamento}%"
    AND f.ciudadPoblacion LIKE "%${ciudad}%"
    AND f.barrio LIKE "%${barrio}%"
    AND gestiondiaria.savedBy LIKE "%${asesor}%"`
}

function MayorVentaAsesor(fecha) {
    return `SELECT gestiondiaria.valorPedido, TRIM(LOWER(gestiondiaria.savedBy)) as asesor FROM gestiondiaria WHERE gestiondiaria.ingresoFH LIKE '%${fecha}%'`
}

function TotalYearAsesor(fecha, asesor) {
    return `SELECT SUM(valorPedido) as total
    FROM gestiondiaria
    WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
    AND gestiondiaria.savedBy LIKE "%${asesor}%"`
}

function SumYearsDistribuidor(fecha,departamento,ciudad,barrio,distribuidor) {
    return `SELECT SUM(valorPedido) as total
    FROM gestiondiaria
    inner join fichacliente f ON f.id = gestiondiaria.idCliente
    WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
    AND f.departamento LIKE "%${departamento}%"
    AND f.ciudadPoblacion LIKE "%${ciudad}%"
    AND f.barrio LIKE "%${barrio}%"
    AND gestiondiaria.distribuidor LIKE "%${distribuidor}%"`
}

function MayorVentaDistribuidor(fecha) {
    return `SELECT gestiondiaria.valorPedido, TRIM(LOWER(gestiondiaria.distribuidor)) as distribuidor FROM gestiondiaria WHERE gestiondiaria.ingresoFH LIKE '%${fecha}%'`
}

function TotalYearDistribuidor(fecha, distribuidor) {
    return `SELECT SUM(valorPedido) as total
    FROM gestiondiaria
    WHERE gestiondiaria.ingresoFH LIKE "%${fecha}%"
    AND gestiondiaria.distribuidor LIKE "%${distribuidor}%"`
}

/* -------------------------------------METHOD------------------------------------ */

router.post('/valorDistribuidor', async (req, res) => {
    const { fecha,departamento,ciudad,barrio,distribuidor } = req.body;

    const query = ValorDistribuidor(fecha,departamento,ciudad,barrio,distribuidor);
    try {
      let result = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      let data = [];
      result.reduce(function (res, value) {
        if (!res[value.name]) {
          res[value.name] = {
            name: value.name,
            valorPedido: 0,
          };
          data.push(res[value.name]);
        }
        res[value.name].valorPedido += parseInt(value.valorPedido);
        return res;
       }, {});
  
      return res.status(200).json({
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/valorDia', async (req, res) => {
    const { fecha,departamento,ciudad,barrio,distribuidor } = req.body;

  const query = ValorDia(fecha,departamento,ciudad,barrio,distribuidor);
  try {
    
    let result = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});

    let data = [];
    result.reduce(function (res, value) {
      if (!res[value.dia]) {
        res[value.dia] = {
          dia: value.dia,
          valorPedido: 0,
        };
        data.push(res[value.dia]);
      }
      res[value.dia].valorPedido += parseInt(value.valorPedido);
      return res;
     }, {});

    return res.status(200).json({
      data,
      message: "Datos obtenidos correctamente",
    });
    

  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
})

router.post('/valorAsesor', async (req, res) => {
    const { fecha,departamento,ciudad,barrio,asesor } = req.body;

    const query = ValorAsesor(fecha,departamento,ciudad,barrio,asesor);
    try {
      
      let result = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
      var data = [];
  
      result.reduce(function (res, value) {
        if (!res[value.name]) {
          res[value.name] = {
            name: value.name,
            valorPedido: 0,
          };
          data.push(res[value.name]);
        }
        res[value.name].valorPedido += parseInt(value.valorPedido);
        return res;
      }, {});
  
      return res.status(200).json({
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/valorDiaAsesor', async (req, res) => {
    const { fecha,departamento,ciudad,barrio,asesor } = req.body;

    const query = ValorDiaASesor(fecha,departamento,ciudad,barrio,asesor);
    try {
      
      let result = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      let data = [];
      result.reduce(function (res, value) {
        if (!res[value.dia]) {
          res[value.dia] = {
            dia: value.dia,
            valorPedido: 0,
          };
          data.push(res[value.dia]);
        }
        res[value.dia].valorPedido += parseInt(value.valorPedido);
        return res;
       }, {});
  
      return res.status(200).json({
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.get('/AllStates', async (req, res) => {
    const query = States();
    try {
      let data = await Cust.sequelize.query(query,{type: sequelize.QueryTypes.SELECT});
      return res.status(200).json({
        data,
        message: "Departamentos obtenidos correctamente",
      });
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/FilterCity', async (req, res) => {
    const { departamento } = req.body;

    const query = FilterCiudad(departamento);
    try {
      
      let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      return res.status(200).json({
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/FilterDistric', async (req, res) => {
    const { departamento, ciudad } = req.body;

    const query = FilterBarrio(departamento,ciudad);
    try {
      
      let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      return res.status(200).json({
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.get('/FilterDistributor', async (req, res) => {
    const query = FilterDistributor();
    try {
      
      let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      return res.status(200).json({
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.get('/FilterAsesor', async (req, res) => {
    const query = FilterAsesor();
  try {
    
    let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});

    return res.status(200).json({
      data,
      message: "Datos obtenidos correctamente",
    });
    

  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
})

router.post('/SumYearsAsesor', async (req, res) => {
    const { fecha,departamento,ciudad,barrio,asesor } = req.body;
    const query = SumYearsAsesor(fecha,departamento,ciudad,barrio,asesor );
  try {
    
    let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});

    return res.status(200).json({
      data,
      message: "Datos obtenidos correctamente",
    });
    

  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
})

router.post('/MayorVentaAsesor', async (req, res) => {
    const { fecha } = req.body;

    const query = MayorVentaAsesor(fecha);
    try {
      
      let result = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});

      let data = [];
        result.reduce(function (res, value) {
        if (!res[value.asesor]) {
          res[value.asesor] = {
            asesor: value.asesor,
            valorPedido: 0,
          };
          data.push(res[value.asesor]);
        }
        res[value.asesor].valorPedido += parseInt(value.valorPedido);
        return res;
        }, {});

       const maxValue = Math.max(...data.map(o => o.valorPedido), 0);
       const dataMax = data.filter(el => el.valorPedido == maxValue);

       const minValue = Math.min(...data.map(o => o.valorPedido));
       const dataMin = data.filter(el => el.valorPedido == minValue);
  
      return res.status(200).json({        
        dataMax,
        dataMin,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/TotalYearAsesor', async (req, res) => {
    const { fecha, asesor } = req.body;

    const query = TotalYearAsesor(fecha,asesor);
    try {      
      let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      return res.status(200).json({        
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/SumYearsDistribuidor', async (req, res) => {
    const { fecha,departamento,ciudad,barrio,distribuidor } = req.body;
    const query = SumYearsDistribuidor(fecha,departamento,ciudad,barrio,distribuidor );
  try {
    
    let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});

    return res.status(200).json({
      data,
      message: "Datos obtenidos correctamente",
    });
    

  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
})

router.post('/MayorVentaDistribuidor', async (req, res) => {
    const { fecha } = req.body;

    const query = MayorVentaDistribuidor(fecha);
    try {
      
      let result = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});

      let data = [];
        result.reduce(function (res, value) {
        if (!res[value.distribuidor]) {
          res[value.distribuidor] = {
            distribuidor: value.distribuidor,
            valorPedido: 0,
          };
          data.push(res[value.distribuidor]);
        }
        res[value.distribuidor].valorPedido += parseInt(value.valorPedido);
        return res;
        }, {});

       const maxValue = Math.max(...data.map(o => o.valorPedido), 0);
       const dataMax = data.filter(el => el.valorPedido == maxValue);

       const minValue = Math.min(...data.map(o => o.valorPedido));
       const dataMin = data.filter(el => el.valorPedido == minValue);
  
      return res.status(200).json({        
        dataMax,
        dataMin,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

router.post('/TotalYearDistribuidor', async (req, res) => {
    const { fecha, distribuidor } = req.body;

    const query = TotalYearDistribuidor(fecha,distribuidor);
    try {      
      let data = await Cust.sequelize.query(query, {type: sequelize.QueryTypes.SELECT});
  
      return res.status(200).json({        
        data,
        message: "Datos obtenidos correctamente",
      });
      
  
    } catch (error) {
      return res.status(500).json({
        error: error.message,
      });
    }
})

module.exports = router;