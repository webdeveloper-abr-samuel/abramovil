const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'usertask',
  {
    taskID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    usr: {
      type: Sequelize.STRING,
      length: 30
    },
    lat: {
      type: Sequelize.STRING,
      length: 20
    },
    lng: {
      type: Sequelize.STRING,
      length: 20
    },
    custID: {
      type: Sequelize.STRING,
      length: 100
    },
    entryDate: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW //defaultValue: Sequelize.literal("NOW()")
    },
    closeDate: {
      type: Sequelize.DATE
    },
    clat: {
      type: Sequelize.STRING,
      length: 20
    },
    clng: {
      type: Sequelize.STRING,
      length: 100
    },
    routeID: {
      type: Sequelize.INTEGER,
    },
    lastAct: {
      type: Sequelize.STRING,
    }
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
)
