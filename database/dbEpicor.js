const Sequelize = require('sequelize');
const dbEpicor = {}
const sequelize = new Sequelize("Epicor10", "SA", "Epicor123", {
    host: '10.1.1.31',
    dialect: 'mssql',
    operatorAliases: false,
    timezone: "-05:00",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    }
})

dbEpicor.sequelize = sequelize
dbEpicor.Sequelize = Sequelize

module.exports = dbEpicor